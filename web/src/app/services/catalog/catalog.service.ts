import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Catalog } from "../../models/catalog.model";
import { first } from "rxjs/operators";
import { AssetInProductService } from "../assetInProduct/asset-in-product.service";
import { ProductInMenuService } from "../productInMenu/product-in-menu.service";
import { AlertService } from "../alert.service";

@Injectable({
  providedIn: "root",
})
export class CatalogService {
  constructor(
    private http: HttpClient,
    private assetInProductService: AssetInProductService,
    private productInMenuService: ProductInMenuService,
    private alertService: AlertService
  ) {}

  getCatalogs() {
    return this.http.get<Catalog[]>(`/api/catalog`);
  }

  addCatalog(catalog: Catalog) {
    return this.http.post<Catalog>(`/api/catalog`, catalog);
  }
  getProduct() {
    return this.http.put<Catalog[]>(`/api/catalog/custom/request`, {
      isMenu: false,
    });
  }
  getMenu() {
    return this.http.put<Catalog[]>(`/api/catalog/custom/request`, {
      isMenu: true,
    });
  }

  getOnlineItems() {
    return this.http.put<Catalog[]>(`/api/catalog/custom/request`, {
      online: true,
    });
  }
  deleteCatalog(idItem: number) {
    return this.http
      .delete(`/api/catalog/${idItem}`)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  addProductOrAsset(tabAssetOrProduct, catalog: Catalog) {
    if (catalog.isMenu) {
      this.productInMenuService.addMultiProductInMenu(
        tabAssetOrProduct,
        catalog.idItem
      );
    } else {
      this.assetInProductService.addMultiAssetInProduct(
        tabAssetOrProduct,
        catalog.idItem
      );
    }
  }
  getItem(idItem) {
    return this.http.get<Catalog>(`/api/catalog/${idItem}`);
  }
  updateCatalog(catalog: any) {
    return this.http.put<Catalog>(`/api/catalog/${catalog.idItem}`, catalog);
  }
}
