import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AssetInProduct } from "src/app/models/assetInProduct.model";
import { first } from "rxjs/operators";
import { AlertService } from "../alert.service";

@Injectable({
  providedIn: "root",
})
export class AssetInProductService {
  apiURL = "http://localhost:3001";
  constructor(private http: HttpClient, private alertService: AlertService) {}

  addAssetInProduct(assetInProduct: AssetInProduct) {
    return this.http
      .post(`/api/assetInProduct`, assetInProduct)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  countAssetInProduct(idAsset: number) {
    return this.http.get<number>(`/api/assetInProduct/count/asset/${idAsset}`);
  }
  getAssetsInProduct(idProduct) {
    return this.http.get<AssetInProduct[]>(
      `/api/assetInProduct/product/${idProduct}`
    );
  }
  addMultiAssetInProduct(assets, idProduct) {
    assets.map((asset) => {
      const assetInProduct = new AssetInProduct(
        asset.id,
        "",
        idProduct,
        "",
        asset.quantity
      );
      this.addAssetInProduct(assetInProduct);
    });
  }
  getAssetComposeProduct(idAsset) {
    return this.http.get<AssetInProduct[]>(
      `/api/assetInProduct/asset/${idAsset}`
    );
  }
  deleteAssetsInProduct(idProduct: number) {
    return this.http.put(`/api/assetInProduct`, { idProduct });
  }
}
