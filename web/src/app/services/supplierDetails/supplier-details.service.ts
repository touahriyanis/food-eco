import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AlertService } from "../alert.service";
import { Supplier } from "src/app/models/supplier.model";
import { SupplierDetails } from "../../models/supplierDetails.model";
import { pipe } from "rxjs";
import { first } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class SupplierDetailsService {
  constructor(private http: HttpClient, private alertService: AlertService) {}
  deleteSupplierDetails(idSupplier: number) {
    return this.http.delete(`/api/supplierDetails/supplier/${idSupplier}`);
  }
  addSupplierDetails(supplier: SupplierDetails) {
    return this.http.post(`/api/supplierDetails`, supplier);
  }
  addMultiSupplierDetails(product: any[], idSupplier: number) {
    product.map((item) => {
      this.addSupplierDetails(new SupplierDetails(0, item.id, idSupplier))
        .pipe(first())
        .subscribe(
          (data) => {},
          (e) => {
            this.alertService.openSnackBar(e.message, "x");
          }
        );
    });
  }
  getSuppliersDetails(idSupplier) {
    return this.http.get<SupplierDetails[]>(
      `/api/supplierDetails/${idSupplier}`
    );
  }
  getAllSuppliersDetails() {
    return this.http.get<SupplierDetails[]>(`/api/supplierDetails`);
  }
}
