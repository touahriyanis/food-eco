import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/models/user.model";
import { Asset } from "../../models/asset.model";
import { first } from "rxjs/operators";
import { AlertService } from "../alert.service";

@Injectable({
  providedIn: "root",
})
export class AssetService {
  constructor(private http: HttpClient, private alertService: AlertService) {}

  updateAsset(asset) {
    return this.http
      .put(`/api/asset/${asset.idAsset}`, asset)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  addAsset(asset: Asset) {
    return this.http.post(`/api/asset`, asset);
  }
  deleteAsset(idAsset: number) {
    return this.http
      .delete(`/api/asset/${idAsset}`)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  public getAssets() {
    return this.http.get<Asset[]>(`/api/asset`);
  }

  public getCurrentEmployees() {
    return this.http.get<User[]>(`/api/employees/current`);
  }

  public getOldEmployees() {
    return this.http.get<User[]>(`/api/employees/ended`);
  }
}
