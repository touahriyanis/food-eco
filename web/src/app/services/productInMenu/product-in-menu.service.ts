import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ProductInMenu } from "../../models/productInMenu.model";
import { AlertService } from "../alert.service";
import { first } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class ProductInMenuService {
  apiURL = "http://localhost:3001";
  constructor(private http: HttpClient, private alertService: AlertService) {}

  addProductInMenu(productInMenu: ProductInMenu) {
    return this.http.post<ProductInMenu>(`/api/menuProduct`, productInMenu);
  }
  getProductInMenu(idMenu: number) {
    return this.http.get<ProductInMenu[]>(`/api/menuProduct/menu/${idMenu}`);
  }
  addMultiProductInMenu(products, idMenu) {
    products.map((product) => {
      const productInMenu = new ProductInMenu(
        idMenu,
        "",
        product.id,
        "",
        product.quantity
      );
      this.addProductInMenu(productInMenu)
        .pipe(first())
        .subscribe(
          (data) => {},
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    });
  }

  deleteProductsInMenu(idMenu: number) {
    return this.http.put<ProductInMenu>(`/api/menuProduct`, { idMenu });
  }
}
