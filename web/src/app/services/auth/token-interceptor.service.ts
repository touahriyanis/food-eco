import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor, HttpErrorResponse } from "@angular/common/http";
import { AuthenticationService } from "./authentication.service";
import { Observable, of, throwError } from "rxjs";
import { Router } from "@angular/router";
import { catchError } from "rxjs/operators";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private injector: Injector, private router: Router) {}

  intercept(req, next) {
    const authService = this.injector.get(AuthenticationService);

    const tokenizedReq = req.clone({
      url: `${environment.apiEndPoint}${req.url}`,
      setHeaders: {
        authorization: `Bearer ${authService.getToken()}`,
      },
    });
    return next
      .handle(tokenizedReq)
      .pipe(catchError((x) => this.handleAuthError(x)));
  }

  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    if (err.status == 403) {
      localStorage.removeItem("token");
      this.router.navigateByUrl(`/home`);
      return of(err.message);
    }
    return throwError(err);
  }
}
