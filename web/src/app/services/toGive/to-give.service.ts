import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ToGive } from "src/app/models/to-give.model";

@Injectable({
  providedIn: "root",
})
export class ToGiveService {
  apiURL = "/api";

  constructor(private http: HttpClient) {}

  public getAll() {
    return this.http.get<ToGive[]>(`${this.apiURL}/togive`);
  }

  public getAssociationToGives() {
    return this.http.get<any[]>(`${this.apiURL}/togive/asso`);
  }

  public getAvailableToGives() {
    return this.http.get<any[]>(`${this.apiURL}/togive/available`);
  }

  public addToGive(toGive: ToGive) {
    return this.http.post(`${this.apiURL}/togive`, toGive);
  }

  public getToGive(id: number) {
    return this.http.get<ToGive>(`${this.apiURL}/togive/${id}`);
  }

  public updateToGive(id: number, toGive: any) {
    return this.http.put<ToGive>(`${this.apiURL}/togive/${id}`, toGive);
  }

  public bookToGive(id: number, toGive: any) {
    return this.http.put<ToGive>(`${this.apiURL}/togive/book/${id}`, toGive);
  }

  public updateStatus(id: number, toGive: any) {
    return this.http.put<ToGive>(`${this.apiURL}/togive/status/${id}`, toGive);
  }

  public deleteToGive(id: number) {
    return this.http.delete<ToGive>(`${this.apiURL}/togive/${id}`);
  }
}
