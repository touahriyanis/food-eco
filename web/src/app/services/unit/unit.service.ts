import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Unit } from "../../models/unit.model";

@Injectable({
  providedIn: "root",
})
export class UnitService {
  constructor(private http: HttpClient) {}

  public getUnits() {
    return this.http.get<Unit[]>(`/api/unit`);
  }
  public getUnit(idUnit: number) {
    return this.http.get<Unit>(`/api/unit/${idUnit}`);
  }
}
