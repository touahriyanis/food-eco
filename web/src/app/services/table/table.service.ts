import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Table } from "src/app/models/table.model";
import { Socket } from "ngx-socket-io";

@Injectable({
  providedIn: "root",
})
export class TableService {
  apiURL = "/api";
  updatedTable = this.socket.fromEvent<any>("updatedTable");

  constructor(private http: HttpClient, private socket: Socket) {}

  public getAll() {
    return this.http.get<Table[]>(`${this.apiURL}/table`);
  }

  public addTable(table: Table) {
    return this.http.post(`${this.apiURL}/table`, table);
  }

  public getTable(id: number) {
    return this.http.get<Table>(`${this.apiURL}/table/${id}`);
  }

  public updateTable(id: number, table: Table) {
    return this.http.put<Table>(`${this.apiURL}/table/${id}`, table);
  }

  public realTimeUpdatedTable() {
    this.socket.emit("updateTable");
  }

  public deleteTable(id: number) {
    return this.http.delete<Table>(`${this.apiURL}/table/${id}`);
  }
}
