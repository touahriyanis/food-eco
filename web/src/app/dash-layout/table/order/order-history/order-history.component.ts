import { Component, OnInit, ViewChild } from "@angular/core";
import { OrderService } from "src/app/services/order/order.service";
import { first } from "rxjs/operators";
import { AlertService } from "src/app/services/alert.service";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { Subscription } from "rxjs/internal/Subscription";
import { OrderLineService } from "src/app/services/orderLine/order-line.service";

@Component({
  selector: "app-order-history",
  templateUrl: "./order-history.component.html",
  styleUrls: ["./order-history.component.css"],
})
export class OrderHistoryComponent implements OnInit {
  ordersRowData: any[] = [];

  displayedColumns: string[] = [
    "id",
    "takeaway",
    "priceTotal",
    "time",
    "details",
  ];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  private _updatedOrder: Subscription;

  constructor(
    private orderService: OrderService,
    private orderLineService: OrderLineService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.launchSubscriptions();
    this.getOrders();
  }

  getOrders() {
    this.orderService
      .getPaidOrders()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            this.ordersRowData = data;
            this.ordersRowData.map((order) => {
              order.takeaway = order.takeaway ? "Emporté" : "Sur place";
            });
          } else {
            this.ordersRowData = [];
          }

          this.dataSource = new MatTableDataSource<any>(this.ordersRowData);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  unpayOrder(idOrder, idTable, isTakeaway) {
    const obj = { isPaid: false };
    this.orderService
      .updateOrder(idOrder, obj)
      .pipe(first())
      .subscribe(
        (order) => {
          this.orderService.realTimeUpdatedOrder({
            idTable,
            isPaid: false,
            isTakeaway: isTakeaway == "Sur place" ? false : true,
          });
          this.getOrders();
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  launchSubscriptions() {
    this._updatedOrder = this.orderLineService.updatedOrder.subscribe(
      (data) => {
        this.getOrders();
      }
    );
  }

  ngOnDestroy(): void {
    this._updatedOrder.unsubscribe();
  }
}
