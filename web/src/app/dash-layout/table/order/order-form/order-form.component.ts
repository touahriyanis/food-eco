import { Component, OnInit } from "@angular/core";
import { OrderService } from "src/app/services/order/order.service";
import { OrderLineService } from "src/app/services/orderLine/order-line.service";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { AlertService } from "src/app/services/alert.service";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { CatalogService } from "src/app/services/catalog/catalog.service";
import { first } from "rxjs/operators";
import { Order } from "src/app/models/order.model";

@Component({
  selector: "app-order-form",
  templateUrl: "./order-form.component.html",
  styleUrls: ["./order-form.component.css"],
})
export class OrderFormComponent implements OnInit {
  orderForm: FormGroup;
  order: Order;
  onlineItems: any[];
  idOrder: number;
  idTable: number;
  isTakeaway = false;
  updateMode: boolean;
  updatedOrder: Order;

  constructor(
    private fb: FormBuilder,
    private orderLineService: OrderLineService,
    private alertService: AlertService,
    private orderService: OrderService,
    private catalogService: CatalogService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getItems();

    if (this.router.url === "/order/takeaway") {
      this.isTakeaway = true;
    }

    this.idOrder = this.route.snapshot.params.idOrder;
    if (this.idOrder) {
      this.updateMode = true;
      this.getOneOrder();
    } else {
      this.idTable = this.route.snapshot.params.idTable;
      this.updateMode = false;
    }

    this.orderForm = this.fb.group({
      items: this.fb.array([
        this.fb.group({
          id: ["", Validators.required],
          quantity: ["1", Validators.required],
          comments: [""],
          OrderLineStatus: ["0"],
        }),
      ]),
    });
  }

  onSubmitForm() {
    if (this.formItems.length == 0) {
      return this.alertService.openSnackBar("La liste est vide", "x");
    }

    const formValue = this.orderForm.value;
    this.createOrder();
    let navigationExtras: NavigationExtras;

    if (!this.updateMode) {
      this.orderService
        .addOrder(this.order)
        .pipe(first())
        .subscribe(
          (data) => {
            this.orderLineService.addMultiOrderLines(
              formValue.items,
              data.idOrder
            );
            this.orderService.realTimeCreatedOrder(data);
            navigationExtras = { state: { data: "Création réussie " } };
            this.router.navigate(["/tables"], navigationExtras);
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    } else {
      navigationExtras = { state: { data: "Mise  à jour reussie " } };
      this.updateOrder();
      this.router.navigate(["/tables"], navigationExtras);
    }
  }

  getItems() {
    this.catalogService
      .getOnlineItems()
      .pipe(first())
      .subscribe(
        (data) => {
          this.onlineItems = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  get formItems() {
    return this.orderForm.get("items") as FormArray;
  }

  addAsset(idItem = null, quantity = null, comments = null, status = null) {
    this.formItems.push(
      this.fb.group({
        id: [
          { value: idItem == null ? "" : idItem, disabled: status > 0 },
          Validators.required,
        ],
        quantity: [
          { value: quantity == null ? "1" : quantity, disabled: status > 0 },
          Validators.required,
        ],
        comments: [
          { value: comments == null ? "" : comments, disabled: status > 0 },
        ],
        OrderLineStatus: [
          { value: status == null ? "0" : status, disabled: status > 0 },
        ],
      })
    );
  }

  deleteAsset(index) {
    this.formItems.removeAt(index);
  }

  createOrder() {
    this.order = {
      idOrder: null,
      takeaway: this.isTakeaway,
      isPaid: false,
      priceTotal: 100,
      idUser: null,
      idEntity: null,
      idTable: this.idTable,
      orderLines: null,
    };
  }

  getOneOrder() {
    this.orderService
      .getOrder(this.idOrder)
      .pipe(first())
      .subscribe(
        (order) => {
          this.updatedOrder = order;
          this.getOrderItems();
          this.deleteAsset(0);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  getOrderItems() {
    this.orderLineService
      .getOrderOrderLines(this.idOrder)
      .pipe(first())
      .subscribe(
        (data) => {
          data.map((orderLine) =>
            this.addAsset(
              orderLine.catalogOrder.idItem,
              orderLine.quantity,
              orderLine.comments,
              orderLine.status
            )
          );
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  updateOrder() {
    this.orderLineService.updateOrderOrderLines(
      this.idOrder,
      this.orderForm.value.items
    );
  }

  deleteOrder() {
    this.orderService
      .deleteOrder(this.idOrder)
      .pipe(first())
      .subscribe(
        (order) => {
          this.orderService.realTimeDeletedOrder(this.idOrder);
          return this.router.navigate(["/tables"]);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  showStatus(status) {
    let comment = "";
    switch (status) {
      case 0:
        comment = "Commande pas encore cuisiné";
        break;
      case 1:
        comment = "Commande en cours de cuisine";
        break;
      case 2:
        comment = "Commande Cuisiné";
        break;
      case 3:
        comment = "Commande Servie";
        break;
      default:
        comment = "Erreur";
    }
    this.alertService.openSnackBar(comment, "x");
  }
}
