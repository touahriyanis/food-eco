import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { OrderLine } from "src/app/models/orderline.model";
import { MatSort } from "@angular/material/sort";
import { OrderLineService } from "src/app/services/orderLine/order-line.service";
import { AlertService } from "src/app/services/alert.service";
import { first } from "rxjs/operators";
import { LotService } from "src/app/services/lot/lot.service";
import { Lot } from "../../models/lot.model";
import { OrderService } from "../../services/order/order.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-list-lot",
  templateUrl: "./list-lot.component.html",
  styleUrls: ["./list-lot.component.css"],
})
export class ListLotComponent implements OnInit {
  displayedColumns: string[] = [
    "id",
    "supplierName",
    "label",
    "quantity",
    "currentQuantity",
    "deliveryDate",
    "expireDate",
    "action",
  ];
  dataSource: MatTableDataSource<any>;
  private _orderLineSubscription: Subscription;
  private _updatedOrders: Subscription;
  private _deletedOrder: Subscription;
  private filterType = "all";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() perLine = true;
  orderLine: OrderLine[];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  msg: string;
  constructor(
    private orderService: OrderService,
    private orderLineService: OrderLineService,
    private lotService: LotService,
    private router: Router,
    private alertService: AlertService
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as { data: string };
    if (state) {
      this.msg = state.data;
      this.alertService.openSnackBar(this.msg, "x");
    }
  }

  ngOnInit() {
    this.getAllLots();

    this._updatedOrders = this.orderLineService.updatedOrder.subscribe(
      (data) => {
        this.getAllLots();
      }
    );
    this._orderLineSubscription = this.orderLineService.updatedOrderLine.subscribe(
      (data) => {
        this.functionToExecute();
      }
    );
    this._deletedOrder = this.orderService.deletedOrders.subscribe((data) => {
      this.functionToExecute();
    });
  }
  public functionToExecute() {
    if (this.filterType == "all") {
      return this.getAllLots();
    } else if (this.filterType == "available") {
      this.loadLotAvailable();
    } else if (this.filterType == "expire") {
    }
  }

  loadLotAvailable() {
    this.filterType = "available";
    this.lotService
      .getAvailableLots()
      .pipe(first())
      .subscribe(
        (data) => {
          this.loadInMatTab(data);
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllLots() {
    this.filterType = "all";

    this.lotService
      .getLots()
      .pipe(first())
      .subscribe(
        (data) => {
          this.loadInMatTab(data);
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  getLotsExpire() {
    this.filterType = "expire";

    this.lotService
      .getExpireLots()
      .pipe(first())
      .subscribe(
        (data) => {
          this.loadInMatTab(data);
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  private loadInMatTab(data: Lot[]) {
    this.dataSource = new MatTableDataSource<Lot>(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) != -1;
    };
  }

  ngOnDestroy(): void {
    this._updatedOrders.unsubscribe();
    this._orderLineSubscription.unsubscribe();
    this._deletedOrder.unsubscribe();
  }
}
