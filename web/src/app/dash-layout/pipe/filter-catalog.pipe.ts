import { Pipe, PipeTransform } from "@angular/core";
import { Catalog } from "../../models/catalog.model";

@Pipe({
  name: "filterCatalog",
})
export class FilterCatalogPipe implements PipeTransform {
  transform(items: Catalog[], term: string): Catalog[] {
    if (!items || !term) {
      return items;
    }

    return items.filter(
      (item) => item.title.toLowerCase().indexOf(term.toLowerCase()) !== -1
    );
  }
}
