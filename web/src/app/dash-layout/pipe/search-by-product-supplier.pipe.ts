import { Pipe, PipeTransform } from "@angular/core";
import { SupplierDetails } from "../../models/supplierDetails.model";
import { Supplier } from "../../models/supplier.model";

@Pipe({
  name: "searchByProductSupplier",
})
export class SearchByProductSupplierPipe implements PipeTransform {
  transform(
    suppliers: Supplier[],
    products: SupplierDetails[],
    term: string
  ): any[] {
    if (!suppliers || !term) {
      return suppliers;
    }

    return suppliers.filter((item) => {
      const dataStr = JSON.stringify(item).toLowerCase();
      const productsFilter = products.filter((product) => {
        const dataStr = JSON.stringify(product).toLowerCase();
        return (
          dataStr.indexOf(term.toLowerCase()) !== -1 &&
          product.idSupplier === item.idSupplier
        );
      });

      return (
        productsFilter.length > 0 || dataStr.indexOf(term.toLowerCase()) !== -1
      );
    });
  }
}
