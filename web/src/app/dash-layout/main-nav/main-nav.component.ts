import { Component, OnInit } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map, shareReplay, first } from "rxjs/operators";
import { AuthenticationService } from "src/app/services/auth/authentication.service";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/auth/user.service";
import { AlertService } from "src/app/services/alert.service";

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  roles: string;
  display: boolean;
}

export const ROUTES: RouteInfo[] = [
  {
    path: "/tables",
    title: "Tables",
    icon: "backup_table",
    class: "fontFamily",
    roles: "admin;server",
    display: false,
  },
  {
    path: "/employees",
    title: "Employés",
    icon: "engineering",
    class: "fontFamily",
    roles: "admin",
    display: false,
  },
  {
    path: "/orderline",
    title: "Commandes",
    icon: "receipt_long",
    class: "fontFamily",
    roles: "admin;preparator",
    display: false,
  },
  {
    path: "/listCatalog",
    title: "Menus",
    icon: "menu_book",
    class: "fontFamily",
    roles: "admin",
    display: false,
  },
  {
    path: "/assets",
    title: "Composants",
    icon: "extension",
    class: "fontFamily",
    roles: "admin",
    display: false,
  },
  {
    path: "/supplier",
    title: "Fournisseurs",
    icon: "add_shopping_cart",
    class: "fontFamily",
    roles: "admin",
    display: false,
  },
  {
    path: "/listLot",
    title: "Stock",
    icon: "all_inbox",
    class: "fontFamily",
    roles: "admin",
    display: false,
  },
  {
    path: "/given",
    title: "Dons",
    icon: "pan_tool",
    class: "fontFamily",
    roles: "admin",
    display: false,
  },
  {
    path: "/donations",
    title: "Association",
    icon: "apartment",
    class: "fontFamily",
    roles: "association",
    display: false,
  },
];

@Component({
  selector: "app-main-nav",
  templateUrl: "./main-nav.component.html",
  styleUrls: ["./main-nav.component.css"],
})
export class MainNavComponent implements OnInit {
  public menuItems: any[];

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {
    this.menuItems = ROUTES.filter((menuItem) => {
      return this.userService
        .isAllowed({ roles: menuItem.roles })
        .pipe(first())
        .subscribe(
          (data) => {
            menuItem.display = data.permission;
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    });
  }

  logout() {
    this.authService.logoutUser();
    this.router.navigate(["/home"]);
  }
}
