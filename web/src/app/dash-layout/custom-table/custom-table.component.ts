import {
  Component,
  Input,
  AfterViewInit,
  ViewChild,
  OnInit,
  OnChanges,
  Output,
  EventEmitter,
} from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
import {
  TablePaginationSettingsModel,
  ColumnSettingsModel,
} from "./table-settings.model";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSortModule, MatSort } from "@angular/material/sort";
import { MatTableModule, MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { FormGroup } from "@angular/forms";
@Component({
  selector: "app-custom-table",
  templateUrl: "./custom-table.component.html",
  styleUrls: ["./custom-table.component.css"],
})
export class CustomTableComponent implements OnInit {
  selectedRowIndex = -1;

  /**
   * @description Column names for the table
   */
  columnNames: string[] = [];
  /**
   * @description enable selection of rows
   */
  @Input() enableCheckbox: boolean;

  @Input() form: FormGroup;

  @Input() rowSelected = false;
  /**
   * @description Allowing/Dis-allowing multi-selection of rows
   */
  @Input() allowMultiSelect: boolean;
  /**
   * @description `sqColumnDefinition` is additional configuration settings provided to `sq-table`.Refer [sqColumnDefinition].
   */
  @Input() sqColumnDefinition: ColumnSettingsModel[];
  /**
   * @description `sqPaginationConfig` is additional configuration settings provided to `sq-table`.Refer [SqTablePaginationSettingsModel].
   */
  @Input() sqPaginationConfig?: TablePaginationSettingsModel;
  /**
   * @description Data which will be displayed in tabular format
   */
  @Input() rowData: object[];
  /**
   * @description variable to store selection data
   */
  selection = new SelectionModel<{}>();
  /**
   * @description Local variable to convert JSON data object to MatTableDataSource
   */
  dataSource: MatTableDataSource<{}>;
  /**
   * @description ViewChild to get the MatSort directive from DOM
   */
  @ViewChild(MatSort) sort: MatSort;
  /**
   * @description ViewChild to get the MatPaginator directive from DOM
   */
  @ViewChild(MatPaginator) paginator: MatPaginator;
  /**
   * @description Lifecycle hook that is called after a component's view has been fully initialized.
   */
  @Output() getSelectedRows = new EventEmitter();

  @Output() id = new EventEmitter();

  @Output() onRowClick = new EventEmitter();

  @Output() onButtonClick = new EventEmitter();

  @Input() buttonName: String;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  /**
   * @hidden
   */
  /**
   * Lifecycle hook that is called when any data-bound property of a datasource changes.
   */
  ngOnChanges() {
    this.dataSource = new MatTableDataSource(this.rowData);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
    this.getSelectedRows.emit(this.selection.selected);
  }

  clickButton(row) {
    this.onButtonClick.emit(row.id);
  }

  /** Gets the selected rows array on row select. */
  rowSelect(row: any) {
    if (row.login) {
      this.rowSelected = true;
      this.onRowClick.emit(true);
      this.id.emit(row.id);
      const object = {
        firstName: row.firstName,
        lastName: row.lastName,
        dateOfBirth: row.dateOfBirth,
        email: row.email,
        login: row.login,
        password: row.password,
        contractStart: row.contractStart,
        contractEnd: row.contractEnd,
        role: "admin",
      };
      this.form.setValue(object);
    }
  }
  /**
   * @hidden
   */
  /**
   * Initialize the directive/component after Angular first displays the data-bound properties
   * and sets the directive/component's input properties
   */
  ngOnInit() {
    for (const column of this.sqColumnDefinition) {
      this.columnNames.push(column.name);
    }
    // Condition to add selection column to the table
    if (this.enableCheckbox) {
      this.columnNames.splice(0, 0, "select");
      this.sqColumnDefinition.splice(0, 0, {
        name: "select",
        displayName: "#",
      });
    }
    // Setting selection model
    this.selection = new SelectionModel<{}>(this.allowMultiSelect, []);
    this.dataSource = new MatTableDataSource(this.rowData);
  }
  /** Highlights the selected row on row click. */
  highlight(row: any) {
    this.selectedRowIndex = row.position;
  }
}
