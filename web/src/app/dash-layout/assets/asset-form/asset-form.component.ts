import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UnitService } from '../../../services/unit/unit.service';
import { Unit } from '../../../models/unit.model';
import { first } from 'rxjs/operators';
import { AlertService } from '../../../services/alert.service';
import { Asset } from '../../../models/asset.model';
import { AssetService } from '../../../services/asset/asset.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-asset-form',
  templateUrl: './asset-form.component.html',
  styleUrls: ['./asset-form.component.css'],
})
export class AssetFormComponent implements OnInit {
  assetsForm: FormGroup;
  units: Unit[];
  @Output() messageEvent = new EventEmitter<Asset>();
  constructor(
    private fb: FormBuilder,
    private unitService: UnitService,
    private alertService: AlertService,
    private assetService: AssetService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.loadUnit();
    this.initForm();
  }
  initForm() {
    this.assetsForm = this.fb.group({
      label: ['', Validators.required],
      idUnit: ['', Validators.required],
    });
  }
  loadUnit() {
    this.unitService
      .getUnits()
      .pipe(first())
      .subscribe(
        (units) => {
          this.units = units;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, 'x');
        }
      );
  }
  onSubmitForm() {
    const value = this.assetsForm.value;
    const asset = new Asset(0, 0, value.label, value.idUnit);
    this.assetService
      .addAsset(asset)
      .pipe(first())
      .subscribe(
        (data) => {
          this.sendMessage(asset);
        },
        (e) => {
          this.alertService.openSnackBar(e.message, 'x');
        }
      );
  }
  sendMessage(asset: Asset) {
    this.messageEvent.emit(asset);
  }
}
