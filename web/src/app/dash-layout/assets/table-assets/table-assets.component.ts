import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { Asset } from "src/app/models/asset.model";
import { AlertService } from "../../../services/alert.service";
import { AssetService } from "../../../services/asset/asset.service";
import { first } from "rxjs/operators";
import { UnitService } from "../../../services/unit/unit.service";
import { Unit } from "../../../models/unit.model";
import { MatPaginator } from "@angular/material/paginator";
import { AssetInProductService } from "../../../services/assetInProduct/asset-in-product.service";

@Component({
  selector: "app-table-assets",
  templateUrl: "./table-assets.component.html",
  styleUrls: ["./table-assets.component.css"],
})
export class TableAssetsComponent implements OnInit {
  displayedColumns: string[] = ["label", "unit", "count", "details", "action"];
  dataSource: MatTableDataSource<any>;

  idInputLabelUpdate: number;
  units: Unit[];
  @Input() label;
  @Input() unit;
  idAssetClick: number;
  tabCountAssetInProduct = new Map();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private assetInProductService: AssetInProductService,
    private alertService: AlertService,
    private assetsService: AssetService,
    private unitService: UnitService
  ) {
    this.getAssets();
  }
  ngOnInit() {
    this.loadUnit();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  loadUnit() {
    this.unitService
      .getUnits()
      .pipe(first())
      .subscribe(
        (units) => {
          this.units = units;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  receiveMessage($event) {
    this.getAssets();
    this.dataSource._updateChangeSubscription();
  }
  displayInput(asset: Asset) {
    if (asset.idAsset == this.idInputLabelUpdate) {
      this.idInputLabelUpdate = null;
      this.label = null;
      this.unit = null;
    } else {
      this.label = asset.label;
      this.unit = asset.idUnit;
      this.idInputLabelUpdate = asset.idAsset;
    }
  }
  updateAsset(asset: Asset) {
    this.idInputLabelUpdate = null;

    this.dataSource.data[
      this.dataSource.data.indexOf(asset)
    ].label = this.label;
    this.dataSource.data[this.dataSource.data.indexOf(asset)].unit = this.unit;

    this.assetsService.updateAsset({
      idAsset: asset.idAsset,
      label: this.label,
      idUnit: this.unit,
    });
    this.label = null;
    this.unit = null;
    this.dataSource._updateChangeSubscription();
  }
  countAssetInProduct(idAsset: number) {
    this.assetInProductService
      .countAssetInProduct(idAsset)
      .pipe(first())
      .subscribe(
        (data) => {
          this.tabCountAssetInProduct.set(idAsset, data);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  deleteAsset(asset: Asset) {
    this.dataSource.data.splice(this.dataSource.data.indexOf(asset), 1);
    this.dataSource._updateChangeSubscription();
    this.assetsService.deleteAsset(asset.idAsset);
  }
  getAssets() {
    this.assetsService
      .getAssets()
      .pipe(first())
      .subscribe(
        (assets) => {
          assets.map((res) => {
            this.countAssetInProduct(res.idAsset);
          });
          this.dataSource = new MatTableDataSource<Asset>(assets);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.dataSource._updateChangeSubscription();
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
}
