import {
  Component,
  OnInit,
  Input,
  ViewChild,
  SimpleChanges,
} from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Unit } from "src/app/models/unit.model";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { AssetInProductService } from "src/app/services/assetInProduct/asset-in-product.service";
import { AlertService } from "src/app/services/alert.service";
import { AssetService } from "src/app/services/asset/asset.service";
import { UnitService } from "src/app/services/unit/unit.service";
import { AssetInProduct } from "../../../models/assetInProduct.model";
import { first } from "rxjs/operators";
import { ChangeDetectionStrategy } from "@angular/core";
@Component({
  selector: "app-asset-compose-product-tab",
  templateUrl: "./asset-compose-product-tab.component.html",
  styleUrls: ["./asset-compose-product-tab.component.css"],
})
export class AssetComposeProductTabComponent implements OnInit {
  displayedColumns: string[] = ["title", "quantity"];
  dataSource: MatTableDataSource<any>;
  @Input() idAsset: number;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private assetInProductService: AssetInProductService,
    private alertService: AlertService,
    private assetsService: AssetService,
    private unitService: UnitService
  ) {}
  ngOnInit() {
    this.getAssetComposeProducts();
  }
  ngOnChanges(changes: SimpleChanges) {
    this.getAssetComposeProducts();
  }
  applyFilter(event: Event) {
    if (this.dataSource) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }
  getAssetComposeProducts() {
    this.assetInProductService
      .getAssetComposeProduct(this.idAsset)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.dataSource.filterPredicate = (data: any, filter) => {
              const dataStr = JSON.stringify(data).toLowerCase();
              return dataStr.indexOf(filter) != -1;
            };
          } else {
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
}
