import { Component, OnInit, ViewChild } from "@angular/core";
import { SupplierDetails } from "src/app/models/supplierDetails.model";
import { Supplier } from "src/app/models/supplier.model";
import { AlertService } from "src/app/services/alert.service";
import { SupplierService } from "src/app/services/supplier/supplier.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SupplierDetailsService } from "src/app/services/supplierDetails/supplier-details.service";
import { first } from "rxjs/operators";
import { MatTableDataSource } from "@angular/material/table";
import { Lot } from "../../models/lot.model";
import { LotService } from "../../services/lot/lot.service";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";

@Component({
  selector: "app-supplier-details",
  templateUrl: "./supplier-details.component.html",
  styleUrls: ["./supplier-details.component.css"],
})
export class SupplierDetailsComponent implements OnInit {
  displayedColumns: string[] = [
    "label",
    "deliveryDate",
    "expireDate",
    "price",
    "currentQuantity",
    "quantity",
  ];
  lot: MatTableDataSource<Lot>;
  idSupplier: number;
  supplier: Supplier;
  supplierDetails: SupplierDetails[];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private alertService: AlertService,
    private supplierService: SupplierService,
    private route: ActivatedRoute,
    private supplierDetailsService: SupplierDetailsService,
    private lotService: LotService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.idSupplier = this.route.snapshot.params.idSupplier;
    this.supplier = new Supplier(this.idSupplier, "", "", 0, 0, 0, "");
    this.getSupplier();
    this.getSupplierDetails();
    this.getLotsBySupplier();
  }
  applyFilter(event: Event) {
    if (this.lot) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.lot.filter = filterValue.trim().toLowerCase();
    }
  }
  getLotsBySupplier() {
    this.lotService
      .getLotBySupplier(this.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            this.lot = new MatTableDataSource(data);
            this.lot.sort = this.sort;
            this.lot.paginator = this.paginator;
            this.lot.filterPredicate = (data: any, filter) => {
              const dataStr = JSON.stringify(data).toLowerCase();
              return dataStr.indexOf(filter) != -1;
            };
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  getSupplier() {
    this.supplierService
      .getSupplier(this.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data == null) {
            this.router.navigate(["/supplier"]);
          }
          this.supplier = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  getSupplierDetails() {
    this.supplierDetailsService
      .getSuppliersDetails(this.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          this.supplierDetails = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
}
