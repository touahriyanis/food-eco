import { Component, OnInit } from "@angular/core";
import { AlertService } from "../../services/alert.service";
import { SupplierService } from "../../services/supplier/supplier.service";
import { SupplierDetailsService } from "../../services/supplierDetails/supplier-details.service";
import { first } from "rxjs/operators";
import { Supplier } from "../../models/supplier.model";
import { SupplierDetails } from "../../models/supplierDetails.model";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import { Lot } from "../../models/lot.model";
import { LotService } from "../../services/lot/lot.service";
import { AssetService } from "../../services/asset/asset.service";
import { Asset } from "../../models/asset.model";
import { ActivatedRoute, NavigationExtras, Router } from "@angular/router";

@Component({
  selector: "app-lot-form",
  templateUrl: "./lot-form.component.html",
  styleUrls: ["./lot-form.component.css"],
})
export class LotFormComponent implements OnInit {
  suppliers: any[];
  supplierDetails: SupplierDetails[];
  lotFormGroup: FormGroup;
  addAssetToSupplierBool = false;
  assets: Asset[];
  idLot: number;
  updateMode = false;
  assetSupplier: string;
  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private supplierService: SupplierService,
    private supplierDetailsService: SupplierDetailsService,
    private lotService: LotService,
    private router: Router,
    private assetService: AssetService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.idLot = this.route.snapshot.params.idLot;
    this.getSuppliers();
    this.initForm();
    if (this.idLot) {
      this.updateMode = true;
      this.patchLotForm();
    }
  }

  private patchLotForm() {
    this.lotService
      .getLot(this.idLot)
      .pipe(first())
      .subscribe(
        (data) => {
          this.lotFormGroup.patchValue({
            assetSupplier: data.idAsset + "-" + data.idSupplier,
            quantity: data.quantity,
            currentQuantity: data.currentQuantity,
            bio: data.bio,
            deliveryDate: data.deliveryDate,
            expireDate: data.expireDate,
            price: data.price,
          });
          this.assetSupplier = data.idAsset + "-" + data.idSupplier;
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  initForm() {
    this.lotFormGroup = this.fb.group({
      assetSupplier: ["", Validators.required],
      quantity: ["", Validators.required],
      currentQuantity: ["", Validators.required],
      bio: [false, Validators.required],
      deliveryDate: ["", Validators.required],
      expireDate: ["", Validators.required],
      price: ["", Validators.required],
    });
  }
  addAssetToSupplier() {
    if (!this.addAssetToSupplierBool) {
      this.addAssetToSupplierBool = true;
      this.getAssets();
      this.lotFormGroup.addControl(
        "idAsset",
        new FormControl("", Validators.required)
      );
      this.lotFormGroup.addControl(
        "idSupplier",
        new FormControl("", Validators.required)
      );
      this.lotFormGroup.removeControl("assetSupplier");
    } else {
      this.addAssetToSupplierBool = false;
      this.getSuppliers();
      this.lotFormGroup.removeControl("idAsset");
      this.lotFormGroup.removeControl("idSupplier");
      this.lotFormGroup.addControl(
        "assetSupplier",
        new FormControl(
          this.updateMode ? this.assetSupplier : "",
          Validators.required
        )
      );
    }
  }
  getAssets() {
    this.assetService
      .getAssets()
      .pipe(first())
      .subscribe(
        (assets) => {
          this.assets = assets;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  getSuppliers() {
    this.supplierService
      .getSuppliers()
      .pipe(first())
      .subscribe(
        (data) => {
          this.suppliers = data;
          this.suppliers.map((item) => {
            this.supplierDetailsService
              .getSuppliersDetails(item.idSupplier)
              .pipe(first())
              .subscribe(
                (data) => {
                  item.supplierDetails = data;
                },
                (e) => {
                  this.alertService.openSnackBar(e.message, "x");
                }
              );
          });
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  onSubmitForm() {
    const formValue = this.lotFormGroup.value;
    const split = this.addAssetToSupplierBool
      ? null
      : formValue.assetSupplier.split("-");
    const idSupplier = this.addAssetToSupplierBool
      ? formValue.idSupplier
      : split[1];
    const idAsset = this.addAssetToSupplierBool ? formValue.idAsset : split[0];
    const lot = new Lot(
      this.updateMode ? this.idLot : 0,
      idAsset,
      idSupplier,
      formValue.deliveryDate,
      formValue.expireDate,
      formValue.price,
      formValue.currentQuantity,
      formValue.quantity,
      formValue.bio
    );
    if (this.addAssetToSupplierBool) {
      this.associateAssetToSupplier(
        new SupplierDetails(0, formValue.idAsset, formValue.idSupplier)
      );
    }
    let navigationExtras: NavigationExtras;
    if (!this.updateMode) {
      navigationExtras = { state: { data: "Création   réussie " } };
      this.lotService
        .addLot(lot)
        .pipe(first())
        .subscribe(
          (data) => {
            this.router.navigate(["/listLot"], navigationExtras);
          },
          (e) => {
            this.alertService.openSnackBar(e.message, "x");
          }
        );
    } else {
      navigationExtras = { state: { data: "Mise à jour réussie  " } };
      this.lotService
        .updateLot(lot)
        .pipe(first())
        .subscribe(
          (data) => {
            this.router.navigate(["/listLot"], navigationExtras);
          },
          (e) => {
            this.alertService.openSnackBar(e.message, "x");
          }
        );
    }
  }

  private associateAssetToSupplier(supplierDetails: SupplierDetails) {
    this.supplierDetailsService
      .addSupplierDetails(supplierDetails)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  getSupplierDetails(idSupplier: number) {
    this.supplierDetailsService
      .getSuppliersDetails(idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          this.supplierDetails = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
}
