import { Component, OnInit } from "@angular/core";
import { AlertService } from "src/app/services/alert.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToGiveService } from "src/app/services/toGive/to-give.service";
import { first } from "rxjs/operators";
import { EntityService } from "src/app/services/entity.service";
import { Entity } from "src/app/models/entity.model";
import { MapsAPILoader } from "@agm/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { GoogleMapsService } from "src/app/services/google-maps/google-maps.service";

declare var google;

@Component({
  selector: "app-donations",
  templateUrl: "./donations.component.html",
  styleUrls: ["./donations.component.css"],
})
export class DonationsComponent implements OnInit {
  lat: number;
  lon: number;
  radius = 1000;

  toGives: any[] = [];
  givingEntites: any[] = [];
  filteredToGives: any[] = [];
  associationToGives: any[] = [];
  filteredAssociationToGives: any[] = [];
  currentFilteredEntity = "";
  filtered = false;
  currentEntity: string;

  associationEntities: any[] = [];

  radiusSearch = false;
  radiusDisplay = "1 Km";
  formGroupSearch: FormGroup;

  searchMode = true;

  constructor(
    private alertService: AlertService,
    private route: ActivatedRoute,
    private entityService: EntityService,
    private toGiveService: ToGiveService,
    private mapsAPILoader: MapsAPILoader,
    private formBuilder: FormBuilder,
    private googleMap: GoogleMapsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.setMapFirstLocation();
    this.getAvailableToGives();
    this.initFormSearch();
    this.getAssociationToGives();
    this.mapsAPILoader.load();
  }

  initFormSearch() {
    this.formGroupSearch = this.formBuilder.group({
      search: "",
    });
  }

  getAvailableToGives() {
    this.toGiveService
      .getAvailableToGives()
      .pipe(first())
      .subscribe(
        (data) => {
          this.toGives = data;
          this.filteredToGives = data;
          this.getGivingEntities();
          if (this.radiusSearch) {
            this.showHideMarkers();
            this.filterToGivesByMarker();
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  getGivingEntities() {
    const list = [];
    this.givingEntites = [];
    if (this.toGives != null) {
      this.toGives.map((toGive) => {
        if (!list.includes(toGive.Entity.idEntity)) {
          toGive.Entity.isShown = true;
          this.givingEntites.push(toGive.Entity);
          list.push(toGive.Entity.idEntity);
        }
      });
    }
  }

  getAssociationEntities() {
    const list = [];
    this.associationEntities = [];
    if (this.associationToGives != null) {
      this.associationToGives.map((toGive) => {
        if (!list.includes(toGive.Entity.idEntity)) {
          toGive.Entity.isShown = true;
          this.associationEntities.push(toGive.Entity);
          list.push(toGive.Entity.idEntity);
        }
      });
    }
  }

  filterToGives(idEntity, name) {
    const list = [];
    if (this.searchMode) {
      this.toGives.map((toGive) => {
        if (toGive.Entity.idEntity == idEntity) {
          list.push(toGive);
        }
      });
      this.filteredToGives = list;
    } else {
      this.associationToGives.map((toGive) => {
        if (toGive.Entity.idEntity == idEntity) {
          list.push(toGive);
        }
      });
      this.filteredAssociationToGives = list;
    }
    this.currentFilteredEntity = name;
    this.filtered = true;
  }

  unfilterToGives() {
    this.filteredToGives = this.toGives;
    this.filteredAssociationToGives = this.associationToGives;
    if (this.radiusSearch) {
      this.filterToGivesByMarker();
      this.showHideMarkers();
    }

    this.currentFilteredEntity = "";
    this.filtered = false;
  }

  filterToGivesByMarker() {
    const list = [];
    const filtered = [];
    if (this.searchMode) {
      this.givingEntites.map((entity) => {
        if (entity.isShown) {
          list.push(entity.idEntity);
        }
      });
      this.toGives.map((toGive) => {
        if (list.includes(toGive.Entity.idEntity)) {
          filtered.push(toGive);
        }
      });
      this.filteredToGives = filtered;
    } else {
      this.associationEntities.map((entity) => {
        if (entity.isShown) {
          list.push(entity.idEntity);
        }
      });
      this.associationToGives.map((toGive) => {
        if (list.includes(toGive.Entity.idEntity)) {
          filtered.push(toGive);
        }
      });
      this.filteredAssociationToGives = filtered;
    }
  }

  setMapFirstLocation() {
    this.entityService
      .getCurrentEntity()
      .pipe(first())
      .subscribe(
        (data) => {
          this.currentEntity = data.name;
          this.lat = data.latitude;
          this.lon = data.longitude;
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  bookToGive(toGive) {
    if (
      confirm(
        `Vous êtes sur de réserver ${toGive.quantity} ${toGive.Lot.Asset.Unit.abrv} de ${toGive.Lot.Asset.label} chez ${toGive.Entity.name} ?`
      )
    ) {
      const obj = { isTaken: true };
      this.toGiveService
        .bookToGive(toGive.idToGive, obj)
        .pipe(first())
        .subscribe(
          (data) => {
            this.getAvailableToGives();
            this.getAssociationToGives();
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    }
  }

  event(type, $event) {
    this.radius = $event;
    this.radiusDisplay = ($event / 1000).toFixed(2) + " Km";
    this.filterToGivesByMarker();
    this.showHideMarkers();
  }

  showHideMarkers() {
    if (this.searchMode) {
      this.givingEntites.forEach((value) => {
        value.isShown = this.googleMap.getDistanceBetween(
          value.latitude,
          value.longitude,
          this.lat,
          this.lon,
          this.radius
        );
      });
    } else {
      this.associationEntities.forEach((value) => {
        value.isShown = this.googleMap.getDistanceBetween(
          value.latitude,
          value.longitude,
          this.lat,
          this.lon,
          this.radius
        );
      });
    }
  }

  changeRadiusSearch(on) {
    this.radiusSearch = on;
    if (this.searchMode) {
      this.givingEntites.map((entity) => {
        if (on) {
          entity.isShown = this.googleMap.getDistanceBetween(
            entity.latitude,
            entity.longitude,
            this.lat,
            this.lon,
            this.radius
          );
          this.filterToGivesByMarker();
        } else {
          entity.isShown = true;
          this.unfilterToGives();
        }
      });
    } else {
      this.associationEntities.map((entity) => {
        if (on) {
          entity.isShown = this.googleMap.getDistanceBetween(
            entity.latitude,
            entity.longitude,
            this.lat,
            this.lon,
            this.radius
          );
          this.filterToGivesByMarker();
        } else {
          entity.isShown = true;
          this.unfilterToGives();
        }
      });
    }
  }

  getAssociationToGives() {
    this.toGiveService
      .getAssociationToGives()
      .pipe(first())
      .subscribe(
        (data) => {
          this.associationToGives = data;
          this.filteredAssociationToGives = data;
          this.getAssociationEntities();
          if (this.radiusSearch) {
            this.showHideMarkers();
            this.filterToGivesByMarker();
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  getFilteredItems() {
    if (this.searchMode) {
      return this.filteredToGives;
    } else {
      return this.filteredAssociationToGives;
    }
  }

  getEntities() {
    if (this.searchMode) {
      return this.givingEntites;
    } else {
      return this.associationEntities;
    }
  }

  changeSearchMode() {
    this.searchMode = !this.searchMode;
    this.changeRadiusSearch(false);
  }

  unbookToGive(toGive) {
    if (
      confirm(
        `Vous êtes sur d'annuler la réservation de ${toGive.quantity} ${toGive.Lot.Asset.Unit.abrv} de ${toGive.Lot.Asset.label} chez ${toGive.Entity.name} ?`
      )
    ) {
      const obj = { status: 0 };
      this.toGiveService
        .updateStatus(toGive.idToGive, obj)
        .pipe(first())
        .subscribe(
          (data) => {
            this.getAvailableToGives();
            this.getAssociationToGives();
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    }
  }

  expiredToGive(toGive) {
    const x = new Date(toGive.Lot.expireDate);
    const today = new Date();

    return x > today;
  }
}
