export class User {
    id: number;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    email: string;
    login: string;
    password: string;
    idEntity: number;
    role: string;
    idRole: number;
    contractStart: Date;
    contractEnd: Date;
}
