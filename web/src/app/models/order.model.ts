import { OrderLine } from './orderline.model';

export class Order {
    idOrder: number;
    takeaway: boolean;
    isPaid: boolean;
    priceTotal: number;
    idUser: number;
    idEntity: number;
    idTable: number;
    orderLines: any[];
}
