export class ProductInMenu {
    constructor(
        public idMenu: number,
        public titleMenu: string,
        public idProduct: number,
        public titleProduct: string,
        public quantity: number,
    ) { }
}
