export class ToGive {
  idToGive: number;
  description: string;
  status: number;
  quantity: number;
  idEntity: number;
  idLot: number;
}
