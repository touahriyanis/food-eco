export class Lot {
    constructor(
        public idLot: number,
        public idAsset: number,
        public idSupplier: number,
        public deliveryDate: Date,
        public expireDate: Date,
        public price: number,
        public currentQuantity: number,
        public quantity: number,
        public bio: boolean
    ) { }
}
