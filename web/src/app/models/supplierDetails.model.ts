export class SupplierDetails {
  constructor(
    public idSupplierDetails: number,
    public idAsset: number,
    public idSupplier: number
  ) {}
}
