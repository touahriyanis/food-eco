import { Component, OnInit } from "@angular/core";
import { User } from "src/app/models/user.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "src/app/services/auth/user.service";
import { AuthenticationService } from "src/app/services/auth/authentication.service";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";
import { AlertService } from "src/app/services/alert.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  user: any;
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  get f() {
    return this.registerForm.controls;
  }

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private fb: FormBuilder,
    private router: Router,
    private alert: AlertService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      login: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.authService
      .login(this.f.login.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (data) => {
          localStorage.setItem("token", data);
          this.userService
            .getCurrentUser()
            .pipe(first())
            .subscribe(
              (data) => {
                this.user = data;
                switch (this.user.Role.grade) {
                  case "admin":
                    this.router.navigate(["/tables"]);
                    break;
                  case "association":
                    this.router.navigate(["/donations"]);
                    break;
                  case "preparator":
                    this.router.navigate(["/orderline"]);
                    break;
                  case "server":
                    this.router.navigate(["/tables"]);
                    break;
                  default:
                    this.router.navigate(["/tables"]);
                    break;
                }
              },
              (e) => {
                this.alert.openSnackBar(e.error.message, "x");
              }
            );
          this.router.navigate(["/"]);
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
  }
}
