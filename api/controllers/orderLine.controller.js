const OrderLineService= require("../services").OrderLineService;

class OrderLineController {
    static async addItemInOrder(req, res) {
        try {
            const orderLine = await OrderLineService.addItemInOrder(req.body.idItem,req.body.idOrder, req.body.quantity, req.body.comments, req.user.idEntity);
            
            return res.status(201).json(orderLine);
        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1]?messageSplit[1]:500;
            const message= messageSplit[0];
            return res.status(status).json({ message: message});
        }


    }
    static async getCurrentOrder(req,res) {
        try {
            const orderLine = await OrderLineService.getCurrentOrder(req.user.idEntity);
            if (!orderLine)
                return res.status(204).end();
            return res.status(200).json(orderLine);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async deleteOrderLine(req, res) {
        try {
            const orderLine = await OrderLineService.deleteOrderLine(req.params.idOrderLine);
            if (!orderLine)
                return res.status(204).end();

            return res.status(200).json(orderLine);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async deleteOrderOrderLines(req, res) {
        try {
            const orderLine = await OrderLineService.deleteOrderOrderLines(req.params.idOrder);
            if (!orderLine)
                return res.status(204).end();

            return res.status(200).json(orderLine);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async updateOrderLine(req, res) {
        try {
            const orderLine = await OrderLineService.updateOrderLine(req.body.idOrderLine, req.body.quantity,req.body.comments,req.body.status,req.body.idOrder);
            if (!orderLine)
                return res.status(204).end();

            return res.status(200).json(orderLine);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async updateStatus(req, res) {
        try {
            const orderLine = await OrderLineService.updateStatus(req.body.idOrder,req.body.idOrderLine,req.body.idItem,req.body.status,req.body.quantity,req.user.idEntity);
            if (!orderLine)
                return res.status(204).end();

            return res.status(200).json(orderLine);

        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1]?messageSplit[1]:500;
            const message= messageSplit[0];
            return res.status(status).json({ message: message});
        }

    }
    static async getOrderOrderLines(req, res) {
        try {
            const orderOrderLine = await OrderLineService.getOrderLineByIdOrder(req.params.idOrder);
            if (orderOrderLine == 0)
                return res.status(204).json({ message: "No product found" });

            return res.status(200).json(orderOrderLine);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getItemSales(req,res) {
        try {
            const itemSales = await OrderLineService.countItemSalePerDay(req.body.startDate, req.body.endDate, req.body.idItem);
            /*if (!itemSales)
                return res.status(204).end();*/
            return res.status(200).json(itemSales);
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: e.message });
        }
    }
}
module.exports = OrderLineController;