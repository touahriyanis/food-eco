const models = require('../models');
const AssetInProductService = require('../services').AssetInProductService;

class AssetInProductController {
  static async addAssetInProduct(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.addAssetInProduct(
        req.body.idProduct,
        req.body.idAsset,
        req.body.quantity
      );
      return res.status(201).json(AssetInProduct);
    } catch (e) {
      const messageSplit = e.message.split('|');
      const status = messageSplit[1] ? messageSplit[1] : 500;
      const message = messageSplit[0];

      return res.status(status).json({ message: message });
    }
  }
  static async updateAssetInProduct(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.updateAssetInProduct(
        req.params.idProduct,
        req.params.idAsset,
        req.body.quantity,
        req.body.newIdAsset
      );
      if (AssetInProduct == 0)
        return res.status(204).json({ message: 'No product in menu update' });

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      const messageSplit = e.message.split('|');
      const status = messageSplit[1] ? messageSplit[1] : 500;
      const message = messageSplit[0];

      return res.status(status).json({ message: message });
    }
  }
  static async countAssetInProduct(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.countAssetInProduct(
        req.params.idAsset
      );

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  static async getAllAssetInProduct(res) {
    try {
      const AssetInProduct = await AssetInProductService.getAllAssetInProduct();
      if (AssetInProduct == 0)
        return res.status(204).json({ message: 'No Product in menu found' });

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  static async AssetComposeProducts(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.AssetComposeProducts(
        req.params.idAsset
      );
      if (AssetInProduct == 0)
        return res.status(204).json({ message: 'No product found' });

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  static async getOneAssetInOneProduct(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.getOneAssetInOneProduct(
        req.params.idAsset,
        req.params.idProduct
      );
      if (AssetInProduct == 0)
        return res.status(204).json({ message: 'No Product in menu found' });

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  static async deleteOneAssetInOneProduct(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.deleteOneAssetInOneProduct(
        req.body.idAsset,
        req.body.idProduct
      );
      if (!AssetInProduct)
        return res.status(204).json({ message: 'No Product in menu delete' });

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  static async getAssetInOneProduct(req, res) {
    try {
      const AssetInProduct = await AssetInProductService.getAssetInOneProduct(
        req.params.idProduct
      );
      if (AssetInProduct == 0)
        return res.status(204).json({ message: 'No menu found' });

      return res.status(200).json(AssetInProduct);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }
}

module.exports = AssetInProductController;
