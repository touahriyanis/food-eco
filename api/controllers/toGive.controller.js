const models = require('../models');
const ToGive = models.ToGive;
const Entity= models.Entity;
const ToGiveService = require('../services').ToGiveService;

class ToGiveController {
    static async addToGive(req, res) {
        try {
            const toGive = await ToGiveService.addToGive(req.body.description, req.body.quantity, req.body.idLot, req.user.idEntity);
            if (!toGive) {
                return res.status(204).end();
            }
            return res.status(201).json(toGive);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async getAllToGive(req,res) {
        try {
            const toGive = await ToGiveService.getAllToGive(req.user.idEntity);
            if (toGive == 0)
                return res.status(204).end();
            return res.status(200).json(toGive);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getAssociationToGive(req,res) {
        try {
            const toGive = await ToGiveService.getAssociationToGive(req.user.idEntity);
            if (toGive == 0)
                return res.status(204).end();
            return res.status(200).json(toGive);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getAvailableToGive(req,res) {
        try {
            const toGives = await ToGiveService.getAvailableToGive();
            if (toGives == 0)
                return res.status(204).end();
            return res.status(200).json(toGives);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getStats(req,res) {
        try {
            const toGives = await ToGiveService.getPercentageStats(req.params.idLot, req.user.idEntity);
            if (toGives == 0)
                return res.status(204).end();
            return res.status(200).json(toGives);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async updateToGive(req, res) {
        try {
            const toGive = await ToGiveService.updateToGive(
                req.params.idToGive,
                req.body.description,
                req.body.quantity,
                req.body.status,
                req.user.idEntity);
            if (!toGive)
                return res.status(204).end();

            return res.status(200).json(toGive);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async bookToGive(req, res) {
        try {
            const toGive = await ToGiveService.bookToGive(req.params.idToGive, req.user.idEntity);
            if (!toGive)
                return res.status(204).end();
            return res.status(200).json(toGive);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async changeStatus(req, res) {
        try {
            const toGive = await ToGiveService.changeStatus(req.params.idToGive, req.body.status, req.body.idLot, req.body.quantity);
            if (!toGive)
                return res.status(204).end();
            return res.status(200).json(toGive);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getToGive(req, res) {
        try {
            const toGive = await ToGiveService.getToGive(req.params.idToGive);
            if (!toGive)
                return res.status(204).end();

            return res.status(200).json(toGive);
        } catch (e) {
            return res.status(500).json({ message: e.message });

        }

    } 

    static async deleteToGive(req, res) {
        try {
            const toGive = await ToGiveService.deleteToGive(req.params.idToGive, req.user.idEntity);
            if (toGive == 0)
                return res.status(204).end();

            return res.status(200).json(toGive);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

}

module.exports = ToGiveController;
