
const OrderService = require("../services").OrderService;
const OrderLineService= require("../services").OrderLineService;
class OrderController {
    static async addOrder(req, res) {
        try {
            const order = await OrderService.addOrder(req.body.takeaway, req.body.isPaid, req.user.id, req.user.idEntity, req.body.idTable);
            if (!order) {
                return res.status(204).end();
            }
            return res.status(201).json(order);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getAllOrder(res) {
        try {
            const order = await OrderService.getAllOrder();
            if (order == 0)
                return res.status(204).end();
            return res.status(201).json(order);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getAllTableOrders(req,res) {
        try {
            const order = await OrderService.getAllTableOrders(req.params.idTable);
            if (order == 0)
                return res.status(204).end();
            return res.status(201).json(order);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getAllPaidOrders(req,res) {
        try {
            const orders = await OrderService.getAllPaidOrders(req.user.idEntity);
            if (orders == 0)
                return res.status(204).end();
            return res.status(201).json(orders);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getTakeawayOrders(req,res) {
        try {
            const order = await OrderService.getTakeawayOrders(req.user.idEntity);
            if (order == 0)
                return res.status(204).end();
            return res.status(201).json(order);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async updateOrder(req, res) {
        try {
            const order = await OrderService.updateOrder(
                req.params.idOrder,
                req.body.takeaway,
                req.body.isPaid,
                req.body.priceTotal);
            if (!order)
                return res.status(204).end();

            return res.status(200).json(order);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }


    }

    static async updateOrderPrice(req, res) {
        try {
            const order = await OrderLineService.updateOrderPrice(req.params.idOrder);
            if (!order)
                return res.status(204).end();

            return res.status(200).json(order);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }


    }

    static async getOneOrder(req, res) {
        try {
            const order = await OrderService.getOneOrder(req.params.idOrder);
            if (!order)
                return res.status(204).end();

            return res.status(200).json(order);
        } catch (e) {
            return res.status(500).json({ message: e.message });

        }

    }
    
    // TODO  pouvoir recuperer les commandes en cours les 3 etats d'une commande sont a definir quand elle est paye
    // en att en cours et donner et payer   

    static async deleteOrder(req, res) {
        try {
            const order = await OrderLineService.deleteOrder(req.params.idOrder);
            if (!order)
                return res.status(204).end();

            return res.status(200).json(order);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async getAffluencePerHours(req,res) {
        try {
            const affluence = await OrderService.affluencePerHours(req.body.date);
            if (affluence == 0)
                return res.status(204).end();
            return res.status(201).json(affluence);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }
    static async getAffluencePerDays(req,res) {
        try {
            const affluence = await OrderService.affluenceBetween(req.body.startDate, req.body.endDate);
            if (affluence == 0)
                return res.status(204).end();
            return res.status(201).json(affluence);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }
}
module.exports = OrderController;