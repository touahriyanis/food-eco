
const SupplierService = require("../services").SupplierService;
class SupplierController {
    static async addSupplier(req, res) {
        const data = req.body;
        try {
            const Supplier = await SupplierService.addSupplier(data.address, data.tel, data.name,data.email,req.user.idEntity);
            if (!Supplier) {
                return res.status(204).end();
            }
            return res.status(201).json(Supplier);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async getAllSupplier(req,res) {
        try {
            const Supplier = await SupplierService.getAllSupplier(req.user.idEntity);
            if (Supplier ==0)
                return res.status(204).end();
            return res.status(200).json(Supplier);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async updateSupplier(req, res) {
        const dataBody = req.body;
        const dataParams = req.params;
        try {
            const Supplier = await SupplierService.updateSupplier(
                dataParams.idSupplier,
                dataBody.address,
                dataBody.tel,
                dataBody.name,
                dataBody.email);
            if (!Supplier)
                return res.status(204).end();

            return res.status(200).json(Supplier);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }


    }

    static async getOneSupplier(req, res) {
        try {
            const Supplier = await SupplierService.getOneSupplier(req.params.idSupplier,req.user.idEntity);
            if (!Supplier)
                return res.status(204).end();

            return res.status(200).json(Supplier);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }
    // TODO  pouvoir recuperer les commandes en cours les 3 etats d'une commande sont a definir quand elle est paye
    // en att en cours et donner et payer   

    static async deleteSupplier(req, res) {
        try {
            const Supplier = await SupplierService.deleteSupplier(req.params.idSupplier,req.user.idEntity);
            if (!Supplier)
                return res.status(204).end();

            return res.status(200).json(Supplier);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

}
module.exports = SupplierController;