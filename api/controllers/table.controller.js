const models = require('../models');
const Table = models.Table;
const Entity= models.Entity;
const TableService = require('../services').TableService;

class TableController {
    static async addTable(req, res) {
        try {
            const table = await TableService.addTable(req.body.description, req.body.name, req.body.numberOfSeats, req.user.idEntity);
            if (!table) {
                return res.status(204).end();
            }
            return res.status(201).json(table);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async getAllTables(req,res) {
        try {
            const table = await TableService.getAllTables(req.user.idEntity);
            if (table == 0)
                return res.status(204).end();
            return res.status(200).json(table);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async updateTable(req, res) {
        try {
            const table = await TableService.updateTable(
                req.params.idTable,
                req.body.description,
                req.body.name,
                req.body.numberOfSeats);
            if (!table)
                return res.status(204).end();

            return res.status(200).json(table);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getTable(req, res) {
        try {
            const table = await TableService.getTable(req.params.idTable);
            if (!table)
                return res.status(204).end();

            return res.status(200).json(table);
        } catch (e) {
            return res.status(500).json({ message: e.message });

        }

    } 

    static async deleteTable(req, res) {
        try {
            const table = await TableService.deleteTable(req.params.idTable);
            if (!table)
                return res.status(204).end();

            return res.status(200).json(table);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

}

module.exports = TableController;
