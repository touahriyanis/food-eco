const models = require('../models');
const SalesItems = models.SalesItems;
const SalesItemService = require('../services').SalesItemService;
const SalesService = require('../services').SalesService;
const CatalogService = require('../services').CatalogService;
class SalesItemsController {

    /**
     * @param idSales
     * @param idItem
     * @returns {Promise<SalesItems>}
     */
    static async AssociateItemToSale(req, res) {
        try {
            const menuSale = await SalesItemService.AssociateItemToSale(req.params.idSale, req.body.id);

            return res.status(201).json(menuSale);
        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1]?messageSplit[1]:500;
            const message= messageSplit[0];
            return res.status(status).json({ message: message });
        }
    }



    /**
     * @param idSales
     * @param idItem
     * @returns {Promise<SalesItems>}
     */
    static async getAllMenus(req, res) {
        try {
            const menus = await SalesItemService.getAllMenus(req.params.idSale);
            if (!menus)
                return res.status(204).end();

            return res.status(200).json(menus);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async deleteAssociation(req, res) {
        try {
            const menuSale = await SalesItemService.deleteAssociation(req.params.idSale, req.body.id);
            if (!menuSale)
                return res.status(204).end();

            return res.status(200).json(menuSale);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

}

module.exports = SalesItemsController;
