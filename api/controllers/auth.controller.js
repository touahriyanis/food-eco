const models = require('../models');
const User = models.User;
const Role = models.Role;
const AuthService = require("../services").AuthService;
const UserService = require("../services").UserService;
const PermitService = require("../services").PermitService;

class AuthController {

    /**
     * inscrire un utilisateur
     * @param firstName
     * @param lastName
     * @param dateOfBirth
     * @param role
     * @param login
     * @param email
     * @param password
     * @returns {Promise<User>}
     */
    static async register(req, res) {

            try {
                const usedRole = await Role.findOne({ where: { grade: req.body.role } });
                if (!usedRole) {
                    return res.status(400).json({ "message": "role not found" });
                }

                if (await AuthService.userExists(req.body.login, req.body.email)) {
                    return res.status(409).json({ 'message': 'user already exists' });
                }

                const user = await AuthService.register(req.body.firstName,
                    req.body.lastName,
                    req.body.dateOfBirth,
                    usedRole,
                    req.body.login,
                    req.body.email,
                    req.body.password,
                    req.body.idEntity,
                    req.body.contractStart ? req.body.contractStart : '2100-12-12',
                    req.body.contractEnd ? req.body.contractEnd : '2100-12-12');
                return res.status(201).json(user);
            } catch (e) {
                return res.status(500).json({ message: e.message });
            }
    }

    /**
     * Logger un user
     * @param user
     * @param password
     * @returns {Promise<string|null>}
     */
    static async login(req,res) {
        try {
            const user = await UserService.getUser(req.body.login);
            if(!user) {
                return res.status(400).json({"message":"login not found"});
            }

            const token = await AuthService.login(user, req.body.password);
            if(token) {
                return res.status(201).json(token);
            } else {
                return res.status(400).json({"message":"Wrong password"});
            }
        } catch(e) {
            return res.status(500).json({message: e.message});
        }
    }

    /**
     * Fermer la session d'un utilisateur
     * @param token
     * @returns {Promise<Session|null>}
     */
    static async logout(token) {
        await Session.destroy({
            where: {
                token
            }
        });
    }

    static async isAllowed(req,res) {
        try {
            const roles = req.body.roles.split(';');
            if (!PermitService.isAllowed(req.token, roles)) {
                return res.status(200).json({permission: false});
            } 
            return res.status(200).json({permission: true});
        } catch (e) {
              return res.status(500).json({ message: e.message });
        }
    }

}

module.exports = AuthController;
