const models = require('../models');
const ProductInMenu = models.ProductInMenu;
const ProductInMenuService = require("../services").ProductInMenuService

class ProductInMenuController {

    static async addProductInMenu(req, res) {
        try {
            
            const productInMenu = await ProductInMenuService.addProductInMenu(req.body.idProduct, req.body.idMenu, req.body.quantity);
            return res.status(201).json(productInMenu);
        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1]?messageSplit[1]:500;
            const message= messageSplit[0];

            return res.status(status).json({ message: message });
        }

    }


    static async updateProductInMenu(req, res) {
        try {
            const productInMenu = await ProductInMenuService.updateProductInMenu(req.params.idProduct, req.params.idMenu, req.body.quantity, req.body.newIdProduct);
            if (productInMenu == 0)
                return res.status(204).json({ message: "No product in menu update" });

            return res.status(200).json(productInMenu);
        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1]?messageSplit[1]:500;
            const message= messageSplit[0];

            return res.status(status).json({ message: message });
        }

    }

    static async getAllMenusDetails(res) {
        try {
            const productInMenu = await ProductInMenuService.getAllMenusDetails();
            if (productInMenu == 0)
                return res.status(204).json({ message: "No Product in menu found" });

            return res.status(200).json(productInMenu);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getMenuDetails(req, res) {
        try {
            const productInMenu = await ProductInMenuService.getMenuDetails(req.params.idMenu);
            if (productInMenu == 0)
                return res.status(204).json({ message: "No product found" });

            return res.status(200).json(productInMenu);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getOneProductInMenu(req, res) {
        try {
            const productInMenu = await ProductInMenuService.getOneProductInMenu(req.params.idMenu, req.params.idProduct);
            if (productInMenu == 0)
                return res.status(204).json({ message: "No Product in menu found" });

            return res.status(200).json(productInMenu);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async deleteProductInMenu(req, res) {
        try {
            const productInMenu = await ProductInMenuService.deleteProductInMenu(req.body.idMenu, req.body.idProduct);
            if (!productInMenu)
                return res.status(204).json({ message: "No Product in menu delete" });

            return res.status(200).json(productInMenu);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async productComposeMenus(req, res) {
        try {
            const productInMenu = await ProductInMenuService.productComposeMenus(req.params.idProduct);
            if (!productInMenu)
                return res.status(204).json({ message: "No menu found" });

            return res.status(200).json(productInMenu);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }


}

module.exports = ProductInMenuController;
