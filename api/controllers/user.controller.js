const models = require('../models');
const User = models.User;
const UserService=require('../services').UserService;
class UserController {
    static async getUsers(res) {
        try {
            const users = await UserService.getUsers();
            if(!users) {
                return res.status(204);
            }
            return res.status(200).json(users);
          } catch (e) {
            return res.status(500).json({message: e.message});
          }
    }

    static async getAllUsers(req, res) {
      try {
          const users = await UserService.getAllUsers(req.user.idEntity);
          if(!users) {
              return res.status(204);
          }
          return res.status(200).json(users);
        } catch (e) {
          return res.status(500).json({message: e.message});
        }
    }

    static async updateUser(req, res) {
      try {
          const user = await UserService.updateUser(req.params.id, req.body.firstName, req.body.lastName, req.body.dateOfBirth, req.body.email,
            req.body.contractStart, req.body.contractEnd, req.body.role);
          if (!user )
            return res.status(204).end();
    
          return res.status(200).json(user);
      } catch (e) {
          return res.status(500).json({ message: e.message });
      }

  }

    static async getCurrentUsers(req, res) {
      try {
          const users = await UserService.getCurrentUsers(req.user.idEntity);
          if(!users) {
              return res.status(204);
          }
          return res.status(200).json(users);
        } catch (e) {
          return res.status(500).json({message: e.message});
        }
    }

    static async getEndedUsers(req, res) {
      try {
          const users = await UserService.getEndedUsers(req.user.idEntity);
          if(!users) {
              return res.status(204);
          }
          return res.status(200).json(users);
        } catch (e) {
          return res.status(500).json({message: e.message});
        }
    }


    static async getRoleUsers(req,res) {
        try {
            const users = await UserService.getRoleUsers(req.params.idRole);
            if(!users) {
              return res.status(204);
            }
            return res.status(200).json(users);
          } catch (e) {
            return res.status(500).json({message: e.message});
          }
    }

}

module.exports = UserController;
