const models = require('../models');
const Sales = models.Sales;


class SalesService {

  /**
   * @param title
   * @param description
   * @param rate
   * @param startDate
   * @param endDate
   * @returns {Promise<Sales>}
   */
  static addSale(title, description, rate, startDate, endDate) {
    try {
      return Sales.create({ title, description, rate, startDate, endDate });
    } catch (e) {
      throw Error('Error while creating Sale');
    }
  }

  /**
   * @param id
   * @param title
   * @param description
   * @param rate
   * @param startDate
   * @param endDate
   * @returns {Promise<Sales>}
   */
  static async updatePromotion(idSale, title, description, rate, startDate, endDate) {
    let object = {};
    let keyTable = ["title", "description", "rate", "startDate", "endDate"];
    for (let i = 1; i < arguments.length; i++) {
      if (arguments[i]!=="")
        object[`${keyTable[i - 1]}`] = arguments[i];
    }
    //TODO blinder ne plus pouvoir update une date anterieur a start ou a ce qui est dans la bdd
    try {
      return await Sales.update(object, { where: { idSale } });
    } catch (e) {
      throw Error('Error while  updating Sale');
    }
  }

  
/**
 * Supprimer une promotion
 * @param {Number} idSale 
 * @returns {Promise<Sales>}
 */
  static async deleteSale(idSale) {
    try {
      return await Sales.destroy({ where: { idSale } });
    } catch (e) {
      throw Error('Error while deleting Sale');
    }
  }

  /**
   *  Recuperer l'ensemble des promotions
   * @returns {Promise<Sales>}
   */
  static async getSales() {
    try {
      return await Sales.findAll();
    } catch (e) {
      throw Error('Error while fetching product in menu data');
    }
  }
  
  /**
   * Recuperer une promotion
   * @param {Number} idSale 
   * @returns {Promise<Sales>}
   */
  static async getOneSale(idSale) {
    try {
      return await Sales.findOne({ where: { idSale } });
    } catch (e) {
      throw Error('Error while fetching Sale');
    }
  }

}

module.exports = SalesService;
