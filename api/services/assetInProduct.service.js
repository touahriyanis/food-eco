const models = require('../models');
const CatalogService = require('./catalog.service');
const AssetInProduct = models.AssetInProduct;
const AssetService = require("./asset.service");
const Catalog = models.Catalog;
const Asset = models.Asset;
class AssetInProductService {

    static async addAssetInProduct(idProduct, idAsset, quantity) {
        try {
            if (!await AssetService.getAsset(idAsset) || !await CatalogService.productExist(idProduct)) {
                throw Error(' idAsset or IdProduct not exist  | 400 ');
            }
            const [assetInProduct, created] = await AssetInProduct.findOrCreate({
                where: { idProduct: idProduct, idAsset: idAsset },
                defaults: {
                    idProduct: idProduct,
                    idAsset: idAsset,
                    quantity: quantity
                }
            });
            if (created == 0)
                throw Error("Association already exist | 400 ")
            return assetInProduct;

        } catch (e) {
            throw Error(e);
        }

    }

    static async updateAssetInProduct(idProduct, idAsset, quantity, newIdAsset) {
       const object={};
        try {
            if (newIdAsset) {
                if (!await AssetService.getAsset(newIdAsset))
                    throw Error('Error newIdAsset not exist in Asset | 400');
                if (await this.getOneAssetInOneProduct(newIdAsset, idProduct)!=0)
                    throw Error('Error this association already exist | 400');
                
                object.idAsset = newIdAsset;
            }


            if (quantity)
                object.quantity = quantity;
            return await AssetInProduct.update(object, { where: { idAsset, idProduct } });
        } catch (e) {
            throw Error(e);
        }

    }
    static async countAssetInProduct(idAsset){
        try{
         return   await  AssetInProduct.count({where:{idAsset}});
        }catch(e){
            console.log(e);
        throw Error('Error while counting asset in product data');
        }
    }
    //TODO est ce que il doit etre possible de changer un id Menu dans AssetInProduct ou il faut juste

    static async getAllAssetInProduct() {
        try {
            return await AssetInProduct.findAll({
                include: [{
                    model: Asset,
                    as: 'assetProduct'
                }, {
                    model: Catalog,
                    as: 'productAsset'
                }]
            });
        } catch (e) {
            throw Error('Error while fetching asset in product data');
        }
    }


    static async getAssetInOneProduct(idProduct) {
        try {
            return await AssetInProduct.findAll({
                where: { idProduct }
                , include: [{
                    model: Asset,
                    as: 'assetProduct'
                }, {
                    model: Catalog,
                    as: 'productAsset'
                }]
            });
        } catch (e) {
            throw Error('Error while fetching product in menu data');
        }
    }


    static async getOneAssetInOneProduct(idAsset, idProduct) {
        try {
            return await AssetInProduct.findAll({
                where: { idAsset, idProduct }
                , include: [{
                    model: Asset,
                    as: 'assetProduct'
                }, {
                    model: Catalog,
                    as: 'productAsset'
                }]
            });
        } catch (e) {
            throw Error('Error while fetching product in menu data');
        }
    }


    static async deleteOneAssetInOneProduct(idAsset, idProduct) {
        try {
            let obj= {};
            if(idAsset)
            obj.idAsset= idAsset;
            if(idProduct)
            obj.idProduct=idProduct;

            return await AssetInProduct.destroy({
                where: obj
            });

        } catch (e) {
            throw Error('Error while deleting product in menu data');
        }
    }


    static async AssetComposeProducts(idAsset) {
        try {
            return await AssetInProduct.findAll({
                where: { idAsset },
                include: [{
                    model: Asset,
                    as: 'assetProduct'
                }, {
                    model: Catalog,
                    as: 'productAsset'
                }]
            });
        } catch (e) {
            throw Error('Error while fetching product in menu data');
        }
    }


}

module.exports = AssetInProductService;
