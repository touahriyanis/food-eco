const models = require('../models');
const Order = models.Order;
const OrderLine = models.OrderLine;
//const OrderLineService = require("./orderLine.service");

class OrderService {
    static async addOrder(takeaway, isPaid, idUser, idEntity, idTable) {
        try {
            return await Order.create({
                takeaway,
                isPaid,
                priceTotal: null,
                idUser,
                idEntity,
                idTable
            });
        } catch (e) {
            throw Error('Error while creating order');
        }
    }

    static async getOneOrder(idOrder) {
        try {
            return await Order.findOne({
                where: {
                    idOrder
                }
            });

        } catch (e) {
            throw Error('Error while fetching order');
        }

    }

    

    // TODO faire une fonction qui permet de faire la somme des item commandé   et ensuite l'implement a l"ajout d'un produit comme ca a chaque ajout le prix total est mis a joiur
    static async getAllOrder() {
        try {
            return await Order.findAll();
        } catch (e) {
            throw Error('Error while fetching order data');
        }

    }

    static async getAllTableOrders(idTable) {
        try {
            return await Order.findAll({where:{idTable, isPaid: false}});
        } catch (e) {
            console.log(e.message);
            throw Error('Error while fetching order data');
        }

    }

    static async getAllPaidOrders(idEntity) {
        try {
            const result = await Order.findAll({
                where: {
                    isPaid:true,
                    idEntity:idEntity
                }
            });
            return result;
        } catch (e) {
            console.log(e);
            throw Error('Error while fetching paid orders data');
        }

    }

    static async getTakeawayOrders(idEntity) {
        try {
            const result = await Order.findAll({
                where: {
                    takeaway:true,
                    isPaid: false,
                    idEntity:idEntity
                }
            });
            return result;
        } catch (e) {
            console.log(e);
            throw Error('Error while fetching takeaway orders data');
        }

    }

    static async updateOrder(id, takeaway, isPaid, priceTotal) {
        let object = {};
        if (takeaway) {
            object.takeaway = takeaway;
        }
        if (typeof isPaid == "boolean") {
            object.isPaid = isPaid;
        }
        if (priceTotal) {
            object.priceTotal = priceTotal;
        }
        try {
            return await Order.update(
                object,
                { where: { idOrder: id } }
            );

        } catch (e) {
            throw Error('Error while updating order');

        }

    }
    static async affluencePerHours(date){
            const sqlRequest = "SELECT COUNT(idOrder) AS affluence ,HOUR(createdAt) AS hour"+
            " FROM `order` WHERE DATE(createdAt)=? GROUP BY HOUR(createdAt)";
            return await models.sequelize.query(sqlRequest,
                {
                    replacements: [date],
                    type: models.sequelize.QueryTypes.SELECT
            });
    }
    static async affluenceBetween(startDate,endDate){
        const sqlRequest = "SELECT COUNT(idOrder) AS affluence,DATE(createdAt) AS day"+
        " FROM `order` WHERE DATE(createdAt) BETWEEN ? AND ?  GROUP BY DATE(createdAt)  ";
        return await models.sequelize.query(sqlRequest,
            {
                replacements: [startDate,endDate],
                type: models.sequelize.QueryTypes.SELECT
        });
}

}
module.exports = OrderService;