const models = require('../models');
const User = models.User;

class UserService {

    /**
     * Récuperer la liste des utilisateurs
     * @returns {Promise<User|null>}
     */
    static async getUsers() {
        try {
            const allUsers = await User.findAll();
            return allUsers;
        } catch (e) {
            throw Error('Error while fetching Users data');
        }
    }

    static async getAllUsers(idEntity) {
        try {
            const sqlRequest = "SELECT * FROM User WHERE idEntity=(:idEntity)";
            return await models.sequelize.query(sqlRequest,
            {
                replacements: {idEntity: idEntity},
                type: models.sequelize.QueryTypes.SELECT
            });
        } catch (e) {
            throw Error('Error while fetching Users data');
        }
    }

    static async getCurrentUsers(idEntity) {
        try {
            const sqlRequest = "SELECT * FROM User WHERE (NOW() BETWEEN contractStart AND contractEnd) AND idEntity=(:idEntity)";
            return await models.sequelize.query(sqlRequest,
            {
                replacements: {idEntity: idEntity},
                type: models.sequelize.QueryTypes.SELECT
            });
        } catch (e) {
            throw Error('Error while fetching Users data');
        }
    }

    static async getEndedUsers(idEntity) {
        try {
            const sqlRequest = "SELECT * FROM User WHERE (NOW() > contractEnd) AND idEntity=(:idEntity)";
            return await models.sequelize.query(sqlRequest,
            {
                replacements: {idEntity: idEntity},
                type: models.sequelize.QueryTypes.SELECT
            });
        } catch (e) {
            throw Error('Error while fetching Users data');
        }
    }

    /**
 * Permet de mettre a jour un produit ou un menu
 * @param {Number} id 
 * @param {String} title 
 * @param {Number} price 
 * @param {boolean} isMenu 
 * @param {boolean} online 
 * @returns {Promise<Entity>}
 */
  static async updateUser(id, firstName, lastName, dateOfBirth, email,
    contractStart, contractEnd, role) {
    let object = {};
    let keyTable = ["firstName", "lastName", "dateOfBirth", "email", "contractStart", "contractEnd", "role"];
    for (let i = 1; i < arguments.length; i++) {
      if (arguments[i])
        object[`${keyTable[i - 1]}`] = arguments[i];
    }
    try {
        return await User.update(
                object,
                { where: { id:id } }
            );
    } catch (e) {
      throw Error('Error while  updating Entity');
    }

  }

    
    /**
     * Récuperer la liste des utilisateurs selon role
     * @param idRole
     * @returns {Promise<User|null>}
     */
    static async getRoleUsers(idRole) {
        try {
            const user = await User.findAll({ where: { idRole } });
            return user;
        } catch (e) {
            throw Error('Error while fetching Users data');
        }
    }
    
    /**
     * Récuperer un utilisateur
     * @param login
     * @returns {Promise<User|null>}
     */
    static async getUser(login) {
        try {
            const user = await User.findOne({ where: { login:login } });
            return user;
        } catch (e) {
            throw Error('Error while fetching User data');
        }
    }
}

module.exports = UserService;
