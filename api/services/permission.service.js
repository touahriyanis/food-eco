const base64 = require('base64url');

class PermitService {

   /**
     * Vérifier le role d'un utilisateur
     * @param token
     * @param roles
     * @returns {boolean}
     */
    static isAllowed(token, roles) {
        try {
            // decoder le role      
            const jwtParts = token.split('.');
            const data = jwtParts[1];
            const decodeddata = base64.decode(data);
            var obj = JSON.parse(decodeddata);

            const role = obj.userRole;
            const isAllowed = roles.includes(role);
            
            if(!isAllowed) {
                return false;
            }
            return true;
        } catch(e) {
            console.log(e);
            throw Error('Error while fetching user token data');
        }

    }

}

module.exports = PermitService;
