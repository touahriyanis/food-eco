const models = require('../models');
const User = models.User;
const SalesItems = models.SalesItems;
const Sales = models.Sales;
const Catalog = models.Catalog;
const SalesService = require("./promotion.service");
const CatalogService = require("./catalog.service");


class SalesItemService {

    static async getOneItemInSale(idSale, idItem) {
        try {
            return await SalesItems.findAll({
                where: { idSale, idItem }
            });
        } catch (e) {
           
            throw Error('Error while fetching item in sale data');
        }
    }
    static async checkRate(idItem, idSale) {
        try {
            const currentRate = await models.sequelize.query("SELECT sum(B.rate) as sumR " +
                " FROM SalesItems A INNER JOIN Sales B ON B.idSale=A.idSale" +
                " WHERE A.idItem=? AND NOW() BETWEEN B.startDate AND B.endDate" +
                " group by idItem  ", {
                replacements: [idItem], type: models.sequelize.QueryTypes.SELECT,
            });
            if (currentRate[0] == undefined)
                return true;
            const data = await Sales.findOne({ where: { idSale } });

            return data.rate + currentRate[0].sumR > 0 && data.rate + currentRate[0].sumR < 1;
        } catch (e) {
            throw Error('Error while fetching sale data');
        }
    }
    /**
     * Récuperer la liste des utilisateurs
     * @returns {Promise<User|null>}
     */
    static async AssociateItemToSale(idSale, idItem) {
        try {
            if (!await CatalogService.getCatalog(idItem) || !await SalesService.getOneSale(idSale))
                throw Error("sale or catalog dont exist |  400 " );

            if (await this.getOneItemInSale(idSale, idItem) != 0)
                throw Error( "Error this association already exist | 400 " );

            if (!await this.checkRate(idItem, idSale))
                throw Error("Error Rate is superior or inferior to 1 | 400" );

            return await SalesItems.create({
                idItem: idItem,
                idSale: idSale
            });
        } catch (e) {
            throw Error(e);
        }
    }


    /**
     * Récuperer la liste des utilisateurs selon role
     * @param idRole
     * @returns {Promise<User|null>}
     */
    static async getAllMenus(idSale) {
        try {
            return await SalesItems.findAll({
                where: {
                    idSale: idSale
                }
            });
        } catch (e) {
            throw Error('Error while fetching Sale Item data');
        }
    }

    /**
     * Récuperer un utilisateur
     * @param login
     * @returns {Promise<User|null>}
     */
    static async deleteAssociation(idSale, idItems) {
        try {
            return await SalesItems.destroy({
                where: {
                    idItem: idItems,
                    idSale: idSale
                }
            });

        } catch (e) {
            throw Error('Error while deleting Item Sale');
        }
    }
}

module.exports = SalesItemService;
