const models = require('../models');
const User = models.User;
const Security = require('../shared').Security;
const base64 = require('base64url');

class AuthService {
  /**
   * inscrire un utilisateur
   * @param firstName
   * @param lastName
   * @param dateOfBirth
   * @param role
   * @param login
   * @param email
   * @param password
   * @returns {Promise<User>}
   */
  static async register(
    firstName,
    lastName,
    dateOfBirth,
    role,
    login,
    email,
    password,
    idEntity,
    contractStart,
    contractEnd
  ) {
    try {
      const hashedPassword = Security.saltHashPassword(password);

      const user = await User.create({
        firstName,
        lastName,
        dateOfBirth,
        login,
        email,
        password: hashedPassword.passwordHash,
        passwordSalt: hashedPassword.salt,
        idRole: role.idRole,
        idEntity: idEntity,
        contractStart,
        contractEnd,
      });

      return user;
    } catch (e) {
      throw Error('Error while creating user');
    }
  }

  /**
   * Logger un user
   * @param user
   * @param password
   * @returns {Promise<string|null>}
   */
  static async login(user, password) {
    try {
      if (
        Security.definedSaltHashPassword(password, user.passwordSalt) !=
        user.password
      )
        return null;
      const token = await Security.generateToken(user);
      return token;
    } catch (e) {
      console.log(e);
      throw Error('Error while logging in');
    }
  }

  /**
   * Fermer la session d'un utilisateur
   * @param token
   * @returns {Promise<Session|null>}
   */
  static async logout(token) {
    await Session.destroy({
      where: {
        token,
      },
    });
  }

  /**
   * Récupérer un utilisateur à partir d'un token
   * @param token
   * @returns {Promise<User|null>}
   */
  static async userFromToken(token) {
    try {
      const jwtParts = token.split('.');
      const data = jwtParts[1];
      const decodeddata = base64.decode(data);
      var obj = JSON.parse(decodeddata);

      const id = obj.userId;
      var user = null;
      if (id) {
        user = await User.findOne({
          where: {
            id: id,
          },
          include: [models.Role],
        });
      }
      return user;
    } catch (e) {
      throw Error('Error while fetching user');
    }
  }

  /**
   * Vérifier si un utilisateur existe déja
   * @param login
   * @param email
   * @returns {Promise<User|null>}
   */
  static async userExists(login, email) {
    try {
      const loginExists = await User.findOne({
        where: {
          login: login,
        },
      });
      const emailExists = await User.findOne({
        where: {
          email: email,
        },
      });
      return !(loginExists === null && emailExists === null);
    } catch (e) {
      throw Error('Error while checking if user exists');
    }
  }
}

module.exports = AuthService;
