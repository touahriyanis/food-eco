const models = require('../models');
const Supplier = models.Supplier;
const GeocodeService = require("./geocode.service");

class SupplierService {
    static async addSupplier(address,tel,name,email,idEntity) {
        try {
            return await GeocodeService.getCoordinates(address).then(async a => {
                if(typeof a[0] == "undefined") throw Error('Invalid address');
                return await Supplier.create({
                    address,
                    name,
                    tel,
                    email,
                    latitude: a[0].latitude,
                    longitude: a[0].longitude,
                    idEntity
                });
            });
        } catch (e) {
            console.log(e);
            if(e.message == 'Invalid address') {
                throw Error('Address is invalid');
              } else {
                throw Error('Error while creating Supplier');
              }
            
        }

    }
// TODO faire une fonction qui permet de faire la somme des item commandé   et ensuite l'implement a l"ajout d'un produit comme ca a chaque ajout le prix total est mis a joiur
    static  async getAllSupplier(idEntity) {
        try {
            return await Supplier.findAll({where:{idEntity}});
        } catch (e) {
            throw Error('Error while fetching Supplier data');
        }

    }

    static async updateSupplier(idSupplier, address,tel,name,email) {
        let object = {};
        let keyTable = ["address", "tel", "name","email"];
        for (let i = 1; i < arguments.length; i++) {
          if (arguments[i]!=="")
            object[`${keyTable[i - 1]}`] = arguments[i];
        }
        try {
            return await Supplier.update(
                object,
                { where: { idSupplier} }
            );

        } catch (e) {
            throw Error('Error while updating Supplier');

        }

    }

    static async getOneSupplier(idSupplier,idEntity) {
        try {
            return await Supplier.findOne({
                where: {
                    idSupplier,
                    idEntity
                }
            });

        } catch (e) {
            console.log(e);
            throw Error('Error while fetching Supplier');

        }

    }

    static async deleteSupplier(idSupplier,idEntity) {
        try {
            return await Supplier.destroy({
                where: {
                    idSupplier,
                    idEntity
                }
            });
            
        } catch (e) {
            throw Error('Error while fetching Supplier');

        }

    }

}
module.exports = SupplierService;