const models = require('../models');
const Unit = models.Unit;

class UnitService {
    static async addUnit(label,abrv) {
        try {
            return await Unit.create({
                label,
                abrv
            });
        } catch (e) {
            throw Error('Error while creating Unit');
        }

    }

    static  async getAllUnit() {
        try {
            return await Unit.findAll();
        } catch (e) {
            throw Error('Error while fetching Unit data');
        }

    }

    static async updateUnit(idUnit,label,abrv) {
        let object = {};
        if (label) {
            object.label =label;
        }
        if (abrv) {
            object.abrv =abrv;
        }
        try {
            return await Unit.update(
                object,
                { where: { idUnit } }
            );

        } catch (e) {
            throw Error('Error while updating Unit');

        }

    }

    static async getOneUnit(idUnit) {
        try {
            return await Unit.findOne({
                where: {
                    idUnit
                }
            });

        } catch (e) {
            throw Error('Error while fetching Unit');

        }

    }

    static async deleteUnit(idUnit) {
        try {
            return await Unit.destroy({
                where: {
                    idUnit
                }
            });
            
        } catch (e) {
            throw Error('Error while fetching Unit');

        }

    }

}
module.exports = UnitService;