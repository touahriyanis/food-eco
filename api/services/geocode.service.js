var NodeGeocoder = require('node-geocoder');
 
var geocoder = NodeGeocoder({
  provider: 'google',
  apiKey: 'AIzaSyCtU_gREM0dlzPhS_9a41F2FmXBK09y7Z4'
});


class GeocodeService {

  static async getCoordinates(address) {
    try {
      return await geocoder.geocode(address);
    } catch (e) {
      throw Error('Address is invalid');
    }

  }


}

module.exports = GeocodeService;
