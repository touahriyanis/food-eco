const models = require('../models');
const Asset= models.Asset;
const Unit= models.Unit;
const UnitService=require("./unit.service")
class AssetService {
 
  static async addAsset(label ,idUnit,idEntity) {
    try {
      if(!await UnitService.getOneUnit(idUnit))
      throw Error("id Unit not exist | 400");

      return await Asset.create({label, idUnit ,idEntity});
    } catch (e) {
      throw Error(e);
    }

  }


  static async updateAsset(idAsset, label,idUnit) {
    let object = {};
    let keyTable = ["label", "idUnit"];
    if(label){
      object.label=label;
    }
    if(idUnit){
      if(!await UnitService.getOneUnit(idUnit))
      throw Error("id Unit not exist | 400");
      object.idUnit=idUnit;
    }
    try {
      return await Asset.update(object, { where: { idAsset } });
    } catch (e) {
      throw Error(e);
    }

  }

 
  static async deleteAsset(idAsset,idEntity) {
    try {
      return await Asset.destroy({ where: { idAsset,idEntity } });
    } catch (e) {
      throw Error('Error while fetching Assets data');
    }

  }


  static async getAssets(idEntity) {
    try {
      return await Asset.findAll({where:{idEntity},include:[Unit]});
    } catch (e) {
      throw Error('Error while fetching Assets data');
    }

  }
  /**
   * Permet de recuperer un assets
   * @param {Number} idAsset
   * @returns {Promise<Assets>}
   */
  static async getAsset(idAsset) {
    try {
      return await Asset.findOne({
        where: { idAsset }
      });
    } catch (e) {
      throw Error('Error while fetching Asset data');
    }

  }


 

}

module.exports = AssetService;
