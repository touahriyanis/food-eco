const models = require('../models');
const Entity = models.Entity;
const GeocodeService = require("./geocode.service");

class EntityService {
/**
 * Permet d'ajouter un produit ou un menu 
 * @param {String} address
 * @param {String} name
 * @param {String} city
 * @param {String} tel
 * @returns {Promise<Entity>}
 */
  static async addEntity(isAssociation, address , name ,city , tel) {
    try {
      return await GeocodeService.getCoordinates(address+' '+city).then(async a => {
        if(typeof a[0] == "undefined") throw Error('Inavlid address');
        return await Entity.create({ isAssociation, address, name, city, latitude: a[0].latitude, longitude: a[0].longitude, tel });
      });
    } catch (e) {
      if(e.message == 'Inavlid address') {
        throw Error('Address is invalid');
      } else {
        throw Error('Error while creating Entity');
      }
    }

  }

/**
 * Permet de mettre a jour un produit ou un menu
 * @param {Number} id 
 * @param {String} title 
 * @param {Number} price 
 * @param {boolean} isMenu 
 * @param {boolean} online 
 * @returns {Promise<Entity>}
 */
  static async updateEntity(id, isAssociation, name, address, city, tel) {
    let object = {};
    let keyTable = ["isAssociation", "name", "address", "city", "tel"];
    for (let i = 1; i < arguments.length; i++) {
      if (arguments[i])
        object[`${keyTable[i - 1]}`] = arguments[i];
    }
    try {
        return await Entity.update(
                object,
                { where: { idEntity:id } }
            );
    } catch (e) {
      throw Error('Error while  updating Entity');
    }

  }

/**
 * Permet de supprimer un produit ou un menu
 * @param {Number} idEntity 
 * @returns {Promise<Entity>}
 */
  static async deleteEntity(idEntity) {
    try {
      return await Entity.destroy({ where: { idEntity } });
    } catch (e) {
      throw Error('Error while fetching entity data');
    }

  }

  /**
   * Permet de recuperer l'ensemble des produits et menus
   * @returns {Promise<Entity>}
   */
  static async getEntities() {
    try {
      return await Entity.findAll();
    } catch (e) {
      throw Error('Error while fetching entity data');
    }
  }
  

  /**
   * Permet de recuperer un produit ou un menu 
   * @param {Number} idEntity 
   * @returns {Promise<Entity>}
   */
  static async getEntity(idEntity) {
    try {
      return await Entity.findOne({ where: { idEntity } });
    } catch (e) {
      throw Error('Error while fetching entity data');
    }

  }
 
}

module.exports = EntityService;
