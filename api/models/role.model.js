module.exports = function (sequelize, DataTypes) {
    const Role = sequelize.define('Role', {
        idRole: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        grade: {
            type: DataTypes.STRING,
            default: 'customer',
            enum: ["customer", "preparer", "admin"],
            allowNull: false
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: false
    });
    return Role;
};
