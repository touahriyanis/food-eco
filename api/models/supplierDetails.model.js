module.exports = function (sequelize, DataTypes) {
    const SupplierDetails = sequelize.define('SupplierDetails', {
        idSupplierDetails: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    }, {
        timestamps:false,
        paranoid: false,
        freezeTableName: true,
        underscored: false
    });
    SupplierDetails.associate = (models) => {
        SupplierDetails.belongsTo(models.Supplier, {
            foreignKey: {
                name: 'idSupplier'
            }
        });
        SupplierDetails.belongsTo(models.Asset, {
            foreignKey: {
                name: 'idAsset'
            }
        });


    };
    return SupplierDetails;
};
