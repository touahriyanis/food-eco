module.exports = function (sequelize, DataTypes) {
    const Session = sequelize.define('Session', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        token: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    Session.associate = (models) => {
        Session.belongsTo(models.User, {
            foreignKey: {
                name: 'idUser',
                allowNull: false
            }
        });
    }
    return Session;
};
