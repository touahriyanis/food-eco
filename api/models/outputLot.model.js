module.exports = function (sequelize, DataTypes) {
    const OutputLot = sequelize.define('OutputLot', {
        idOutputLot: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        quantity: {
            type: DataTypes.FLOAT,
            allowNull: false
        }
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    OutputLot.associate = (models) => {
        OutputLot.belongsTo(models.Lot, {
            foreignKey: {
                name: 'idLot'
            }
        });
    };
    return OutputLot;
};
