module.exports = function (sequelize, DataTypes) {
    const Order = sequelize.define('Order', {
        idOrder: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        takeaway: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        isPaid: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        priceTotal: {
            type: DataTypes.FLOAT,
            allowNull: true
        }
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    Order.associate = (models) => {
        Order.belongsTo(models.User,{
            foreignKey: {
                name: "idUser",
                allowNull: true
            }, onDelete: 'cascade' 
        });
        Order.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idEntity'
            },onDelete: 'cascade' 
        });
        Order.belongsTo(models.Table, {
            foreignKey: {
                name: 'idTable'
            },onDelete: 'cascade' 
        });
    }
    return Order;
};
