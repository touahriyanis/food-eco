module.exports = function (sequelize, DataTypes) {
    const Table = sequelize.define('Table', {
        idTable: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        numberOfSeats: {
            type: DataTypes.BIGINT,
            allowNull: false
        }
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: false
    });
    Table.associate = (models) => {
        Table.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idEntity'
            },onDelete: 'cascade' 
        });
    };
    return Table;
};
