const bodyParser = require('body-parser');
const OrderController = require('../controllers').OrderController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
    app.post('/api/order', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if ((typeof req.body.takeaway == "boolean") && (typeof req.body.isPaid  == "boolean")) {
            return await OrderController.addOrder(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/order/price/:idOrder', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        return await OrderController.updateOrderPrice(req, res);
    });

    app.put('/api/order/:idOrder', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idOrder && (req.body.takeaway || req.body.status || req.body.priceTotal || (typeof req.body.isPaid === "boolean"))) {
            return await OrderController.updateOrder(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/order', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        return await OrderController.getAllOrder(res);
    });

    app.get('/api/order/table/:idTable', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin',, 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        return await OrderController.getAllTableOrders(req,res);
    });

    app.get('/api/order/paid', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'server'])], bodyParser.json(), async (req, res) => {
        return await OrderController.getAllPaidOrders(req,res);
    });

    app.get('/api/order/takeaway', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        return await OrderController.getTakeawayOrders(req,res);
    });

    app.get('/api/order/:idOrder', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        return await OrderController.getOneOrder(req, res);
    });

    app.post('/api/order/affluence/hour', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.body.date) {
            return await OrderController.getAffluencePerHours(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.post('/api/order/affluence/day', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.body.startDate && req.body.endDate) {
            return await OrderController.getAffluencePerDays(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.delete('/api/order/:idOrder', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idOrder) {
            return await OrderController.deleteOrder(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

};
