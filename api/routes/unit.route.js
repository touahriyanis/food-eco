const bodyParser = require('body-parser');
const UnitController = require('../controllers').UnitController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
    app.post('/api/unit', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.body.label && req.body.abrv) {
            return await UnitController.addUnit(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/unit/:idUnit', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idUnit && (req.body.label || req.body.abrv)) {
            return await UnitController.updateUnit(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    app.get('/api/unit',  bodyParser.json(), async (req, res) => {
        return await UnitController.getAllUnit(res);
    });
    app.get('/api/unit/:idUnit', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await UnitController.getOneUnit(req, res);
    });


    app.delete('/api/unit/:idUnit', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idUnit) {
            return await UnitController.deleteUnit(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


};
