const bodyParser = require('body-parser');
const AssetController = require('../controllers').AssetController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {

  app.post('/api/asset', [AuthMiddleware.auth(),PermitMiddleware.permit('admin'),bodyParser.json()], async (req, res) => {
    if (req.body.label && req.body.idUnit && req.user.idEntity) {
      return await AssetController.addAsset(req, res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  app.put('/api/asset/:idAsset', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.idAsset && (req.body.label || req.body.idUnit)) {
      return await AssetController.updateAsset(req, res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  app.get('/api/asset', [AuthMiddleware.auth(),PermitMiddleware.permit('admin'), bodyParser.json()], async (req, res) => {
    if(req.user.idEntity){
       return await AssetController.getAssets(req,res);
    }else{
      return res.status(400).json({ message: "Invalid parameter" });
    }
   

  });


  app.get('/api/asset/:idAsset', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
    return await AssetController.getAsset(req, res);
  });

  app.delete('/api/asset/:idAsset',  [AuthMiddleware.auth(),PermitMiddleware.permit('admin'), bodyParser.json()], async (req, res) => {
    if (req.params.idAsset && req.user.idEntity) {
      return await AssetController.deleteAsset(req, res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

};
