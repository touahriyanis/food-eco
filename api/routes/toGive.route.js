const bodyParser = require('body-parser');
const ToGiveController = require('../controllers').ToGiveController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
     app.post('/api/togive', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.body.description && req.body.quantity && req.body.idLot) {
            return await ToGiveController.addToGive(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/togive/:idToGive', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.params.idToGive && (req.body.description || req.body.quantity || (req.body.status) || req.body.idLot)) {
            return await ToGiveController.updateToGive(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/togive/book/:idToGive', [AuthMiddleware.auth(), PermitMiddleware.permit(['association'])], bodyParser.json(), async (req, res) => {
        return await ToGiveController.bookToGive(req, res);
    });

    app.put('/api/togive/status/:idToGive', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'association'])], bodyParser.json(), async (req, res) => {
        return await ToGiveController.changeStatus(req, res);
    });

    app.get('/api/togive', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'association'])], bodyParser.json(), async (req, res) => {
        return await ToGiveController.getAllToGive(req,res);
    });

    app.get('/api/togive/asso', [AuthMiddleware.auth(), PermitMiddleware.permit(['association'])], bodyParser.json(), async (req, res) => {
        return await ToGiveController.getAssociationToGive(req,res);
    });

    app.get('/api/togive/available', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'association'])], bodyParser.json(), async (req, res) => {
        return await ToGiveController.getAvailableToGive(req,res);
    });

    app.get('/api/togive/stats/:idLot', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator'])], bodyParser.json(), async (req, res) => {
        if (req.params.idLot) {
            return await ToGiveController.getStats(req,res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/togive/:idToGive', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator'])], bodyParser.json(), async (req, res) => {
        if (req.params.idToGive) {
            return await ToGiveController.getToGive(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.delete('/api/togive/:idToGive', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.params.idToGive) {
            return await ToGiveController.deleteToGive(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

};