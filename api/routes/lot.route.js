const bodyParser = require('body-parser');
const LotController = require('../controllers').LotController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
    app.post('/api/lot',[AuthMiddleware.auth(), PermitMiddleware.permit('admin')],bodyParser.json(), async (req, res) => {
        if (req.body.expireDate && req.body.deliveryDate &&  req.body.price>=0 && req.body.quantity>=0 && req.body.idAsset  ) {
            return await LotController.addLot(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/lot/:idLot',[AuthMiddleware.auth(), PermitMiddleware.permit('admin')],bodyParser.json(), async (req, res) => {
        if (req.params.idLot && (req.body.expireDate || req.body.deliveryDate ||  req.body.price >=0 
            || req.body.quantity>=0 || req.body.idAsset || req.body.idSupplier || typeof req.body.bio=="boolean"
            || req.body.currentQuantity>=0)) {
            return await LotController.updateLot(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    app.put('/api/lot/calc/calcCurrentQuantityRouter',[AuthMiddleware.auth(), PermitMiddleware.permit('admin')],bodyParser.json(), async (req, res) => {
        if (req.body.idItem && req.body.quantityOrder &&  req.body.operator) {
            return await LotController.calcCurrentQuantityRouter(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/lot', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await LotController.getLots(req,res);
    });

    app.get('/api/lot/asset', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await LotController.getLotsAssetUnit(req,res);
    });

    app.get('/api/lot/isAvailable', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await LotController.getAvailableLots(req,res);
    });

    app.get('/api/lot/expire', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await LotController.getExpireLots(req,res);
    });

    app.get('/api/lot/supplier/:idSupplier', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.params.idSupplier) {
        return await LotController.getLotsBySupplier(req,res);
        }else{
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/lot/:idLot', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await LotController.getLot(req, res);
    });
    app.get('/api/lot/isAvailable/item/:idItem', bodyParser.json(), async (req, res) => {
        return await LotController.isAvailableRouter(req, res);
    });

    app.post('/api/lot/stats/day/:idLot', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.body.startDate && req.body.endDate && req.params.idLot) {
            return await LotController.getStatsPerDay(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.delete('/api/lot/:idLot', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idLot) {
            return await LotController.deleteLot(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


};
