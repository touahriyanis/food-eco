const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

var NodeGeocoder = require('node-geocoder');
 
var geocoder = NodeGeocoder({
  provider: 'google',
  apiKey: 'AIzaSyCtU_gREM0dlzPhS_9a41F2FmXBK09y7Z4'
});

module.exports = function(app) {

    app.post('/api/test', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], async (req, res) => {
        const a = await geocoder.geocode('29 champs elysée paris');
        console.log(a);
        res.status(200).end();
    });



};
