const bodyParser = require('body-parser');
const SalesController = require('../controllers').SalesController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
  app.post('/api/sale', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.body.title && req.body.description && req.body.rate && req.body.startDate
      && req.body.endDate) {
        return await SalesController.addSale(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  app.put('/api/sale/:idSale', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.idSale && (req.body.title || req.body.description || req.body.rate
      || req.body.startDate || req.body.endDate)) {
        return  await SalesController.updatePromotion(req,res);

    } else {
      return res.status(400).json({ message: "Invalid parameter" })
    };
  });

  app.get('/api/sales', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {

     return await SalesController.getSales(res);

  });
  app.get('/api/sales/:idSale', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
    return await SalesController.getOneSale(req,res);
 });


  app.delete('/api/sale/:idSale', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.idSale) {
        return  await SalesController.deleteSale(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

};
