const bodyParser = require('body-parser');
const ProductInMenuController = require('../controllers').ProductInMenuController;
const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {

    /**
     * Permet d'ajouter un produit à un menu
     * @param idMenu: body
     * @param idProduct: body
     * @param quantity : body
     */
    app.post('/api/menuProduct',bodyParser.json(), async (req, res) => {
        if (req.body.hasOwnProperty("idMenu") && req.body.hasOwnProperty("idProduct") && req.body.quantity>0) {
            return await ProductInMenuController.addProductInMenu(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    /**
     * Permet de mettre  a jour un produit dans un menu ou un menu dans un produit
     * @param idMenu: url
     * @param idProduct :url
     * @param quantity :body
     * @param newIdMenu:body
     * @param newIdProduct : body
     */
    app.put('/api/menuProduct/idMenu/:idMenu/idProduct/:idProduct', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')],bodyParser.json(), async (req, res) => {
        if (req.params.idMenu && req.params.idProduct && (req.body.quantity>0 || req.body.newIdProduct)) {
            return await ProductInMenuController.updateProductInMenu(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    /**
     * Permet de recuperer les details d'un menu
     * @param idMenu:url
     */
    app.get('/api/menuProduct/menu/:idMenu', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin','server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idMenu) {
            return await ProductInMenuController.getMenuDetails(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    /**
     * Permet de recuperer les menus qui sont composés d'un produit
     * @param idProduct:url
     */
    app.get('/api/menuProduct/product/:idProduct', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin','server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idProduct) {
            return await ProductInMenuController.productComposeMenus(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    /**
     * recuperer l'ensemble des menus
     */
    app.get('/api/menuProduct', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin','server'])], bodyParser.json(), async (req, res) => {
        return await ProductInMenuController.getAllMenusDetails(res);
    });
    /**
     * Recuperer un produit avec un menu 
     * @param idProduct : url
     * @param idMenu : url
     */
    app.get('/api/menuProduct/idMenu/:idMenu/idProduct/:idProduct', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin','server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idProduct && req.params.idMenu) {
            return await ProductInMenuController.getOneProductInMenu(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    /**
     * Permet de supprimer un produit dans un menu
     * @param idProduct : url
     * @param idMenu : url
     */
    app.put('/api/menuProduct',[AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        if (req.body.idMenu || req.body.idProduct) {
            return await ProductInMenuController.deleteProductInMenu(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


};
