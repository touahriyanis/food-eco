const bodyParser = require('body-parser');
const TableController = require('../controllers').TableController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
     app.post('/api/table', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        if (req.body.description && req.body.name && req.body.numberOfSeats) {
            return await TableController.addTable(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/table/:idTable', [AuthMiddleware.auth(),PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idTable && (req.body.description || req.body.name || req.body.numberOfSeats)) {
            return await TableController.updateTable(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/table', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        return await TableController.getAllTables(req,res);
    });

    app.get('/api/table/:idTable', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        return await TableController.getTable(req, res);
    });

    app.delete('/api/table/:idTable', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator','server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idTable) {
            return await TableController.deleteTable(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

};
