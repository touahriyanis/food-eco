const bodyParser = require('body-parser');
const SupplierDetailsController = require('../controllers').SupplierDetailsController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {

  app.post('/api/supplierDetails', [AuthMiddleware.auth(),bodyParser.json(),PermitMiddleware.permit('admin')], async (req, res) => {
    if (req.body.idAsset && req.body.idSupplier) {
      return await SupplierDetailsController.addSupplierDetails(req, res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });



  app.get('/api/supplierDetails', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    return await SupplierDetailsController.getAllSupplierDetails(req, res);
  });
 
  app.get('/api/supplierDetails/:idSupplier', [AuthMiddleware.auth(),PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    return await SupplierDetailsController.getSupplierDetails(req, res);
  });

  app.delete('/api/supplierDetails/:idSupplierDetails',  [AuthMiddleware.auth(),PermitMiddleware.permit('admin'), bodyParser.json()], async (req, res) => {
    if (req.params.idSupplierDetails) {
      return await SupplierDetailsController.deleteSupplierDetails(req, res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  app.delete('/api/supplierDetails/supplier/:idSupplier',  [AuthMiddleware.auth(),PermitMiddleware.permit('admin'), bodyParser.json()], async (req, res) => {
    if (req.params.idSupplier) {
      return await SupplierDetailsController.deleteSupplierDetailsBySupplier(req, res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

};
