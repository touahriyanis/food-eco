const bodyParser = require('body-parser');
const CatalogController = require('../controllers').CatalogController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
  /**
   *  permet de creer un menu
   * @param  title :  body 
   * @param price : body
   * @param online : body
   * @param isMenu :body
   */
  app.post('/api/catalog',  [AuthMiddleware.auth(),PermitMiddleware.permit('admin'),bodyParser.json()], async (req, res) => {
  
    if (req.body.hasOwnProperty("title") && req.body.hasOwnProperty("price") && req.body.hasOwnProperty("online") && req.body.hasOwnProperty("isMenu") 
    && req.body.hasOwnProperty("promote") && req.user.idEntity) {
      return await CatalogController.addCatalog(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  /**
   * mettre a jour un produit ou un menu
   * @param idMenu:  url 
   * @param price : body
   * @param title: body
   */
  app.put('/api/catalog/:idItem',  [AuthMiddleware.auth(),PermitMiddleware.permit('admin'),bodyParser.json()], async (req, res) => {
    if (req.params.idItem && (req.body.hasOwnProperty("title") || req.body.hasOwnProperty("price")|| req.body.hasOwnProperty("online") || req.body.hasOwnProperty("isMenu") || req.body.hasOwnProperty("promote"))) {
       return await CatalogController.updateCatalog(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  /**
   * Recuperer l'ensemble des menus et produits
   */
  app.get('/api/catalog',  [AuthMiddleware.auth(),PermitMiddleware.permit(['admin', 'preparator','server']),bodyParser.json()], async (req, res) => {
    if(req.user.idEntity){
      return  await CatalogController.getCatalogs(req,res);
    }else{
      return res.status(400).json({ message: "Invalid parameter" });
    }
     

  });

  /**
   * permet de faire une requete personnalisé en jouant avec online et isMenu
   */
  app.put('/api/catalog/custom/request', [AuthMiddleware.auth(),PermitMiddleware.permit(['admin', 'preparator','server']),bodyParser.json()], async (req, res) => {
    if (req.body.hasOwnProperty("online") || req.body.hasOwnProperty("isMenu") && req.user.idEntity) {
        return await CatalogController.getCatalogCustom(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

  /**
   * permet de recuperer un menu
   * @param idItem:  url
   */
  app.get('/api/catalog/:idItem', [AuthMiddleware.auth(),PermitMiddleware.permit(['admin', 'preparator','server']),bodyParser.json()], async (req, res) => {
    if(req.user.idEntity){
      return await CatalogController.getCatalog(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
        
  });


 

  /**
   *  Suppression  d'un produit ou d'un menu
   * @param idMenu: url
   */
  app.delete('/api/catalog/:idItem', [AuthMiddleware.auth(),bodyParser.json()], async (req, res) => {
    if (req.params.idItem && req.user.idEntity) {
        return await CatalogController.deleteCatalog(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });

};
