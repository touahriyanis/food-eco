const bodyParser = require('body-parser');
const UserController = require('../controllers').UserController;
const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {

  /**
   * Permet de récuperer des utilisateurs 
   */
  app.get('/api/users', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        return await UserController.getUsers(res);
  });

  app.put('/api/users/:id', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.id && (req.body.firstName || req.body.lastName || req.body.dateOfBirth || req.body.email || req.body.contractStart || req.body.contractEnd || req.body.role)) {
        return await UserController.updateUser(req, res);
    } else {
        return res.status(400).send("Invalid parameter").end();
    }
  });

  /**
   * Permet de récuperer des utilisateurs 
   */
  app.get('/api/user', [AuthMiddleware.auth()], bodyParser.json(), async (req, res) => {
      return res.status(200).json(req.user);
  });

  /**
   * Permet de récuperer des utilisateurs 
   */
  app.get('/api/employees',  [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    return await UserController.getAllUsers(req, res);
  });

  /**
   * Permet de récuperer des utilisateurs 
   */
  app.get('/api/employees/current', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    return await UserController.getCurrentUsers(req, res);
  });

  /**
   * Permet de récuperer des utilisateurs 
   */
  app.get('/api/employees/ended', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    return await UserController.getEndedUsers(req, res);
  });

  /**
   * Permet de récuperer des utilisateurs selon role
   */
  app.get('/api/users/:idRole', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    return await UserController.getRoleUsers(req,res);
  });

};
