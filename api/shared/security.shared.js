const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const models = require('../models');
const Role = models.Role;
require('dotenv').config();

class Security {
  /**
   * générer un salt
   * @function
   * @param {number} length - Length of the random string.
   */
  static genRandomString(length) {
    return crypto
      .randomBytes(Math.ceil(length / 2))
      .toString('hex')
      .slice(0, length);
  }

  /**
   * hasher un string avec sha512 et un salt.
   * @function
   * @param {string} password
   * @param {string} salt
   * @returns {Object}
   */
  static sha512(password, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    var value = hash.digest('hex');
    return {
      salt: salt,
      passwordHash: value,
    };
  }

  /**
   * hasher le mdp avec sha512 et un salt aléatoire.
   * @function
   * @param {string} userpassword - Mdp de l'utilisateur.
   * @returns {Object}
   */
  static saltHashPassword(userpassword) {
    var salt = this.genRandomString(16);
    var passwordData = this.sha512(userpassword, salt);
    return passwordData;
  }

  /**
   * hasher le mdp avec sha512 et un salt prédéfinie.
   * @function
   * @param {string} userpassword - Mdp de l'utilisateur.
   * @param {string} salt - le Salt.
   * @returns {string}
   */
  static definedSaltHashPassword(userpassword, salt) {
    var passwordData = this.sha512(userpassword, salt);
    return passwordData.passwordHash.toString();
  }

  /**
   * générer un token avec jwt.
   * @function
   * @param {User} user - Utilisateur.
   * @returns {Promise<string>}
   */
  static async generateToken(user) {
    const userRole = await Role.findOne({
      where: {
        idRole: user.idRole,
      },
    });

    return new Promise((resolve, reject) => {
      let expiration = null;
      switch (user.idRole) {
        case 1: //admin
          expiration = '12h';
          break;
        case 2: //serveur
          expiration = '8h';
          break;
        case 3: //chef cuisisner
          expiration = '6h';
          break;
        default:
          expiration = '2h';
      }

      const j = jwt.sign(
        {
          userId: user.id,
          userRole: userRole.grade,
        },
        `${process.env.JWT_SECRET_KEY}`,
        {
          expiresIn: expiration,
        }
      );

      resolve(j);
    });
  }
}

module.exports = Security;
